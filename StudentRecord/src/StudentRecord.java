
public class StudentRecord {
    
    private static final int  FINAL_MAX = 100;
    private static final int MIDTERM_MAX = 100;
    private static final int FINAL_MIN = 0;
    private static final int MIDTERM_MIN = 0 ;
    private static final double FINAL_WEIGHT =.65;
    private static final double MIDTERM_WEIGHT = .35;
    
    
    int finalExam;
    int midtermExam;

    
    
    public boolean setFinalExam(int score) {
    	this.finalExam = score;
    	return true;
    }
    
    public boolean setMidternExam(int score) {
    	this.midtermExam = score;
    	return true;
    }
    
    public int getFinalExam() {
    	return this.finalExam;
    }
    
    public int setMidternExam() {
    	return this.midtermExam;
    }
    
    public String toString() {
    	return "Your score is" + finalExam;
    }
    
    public boolean equals(int score1, int score2) {
    	if(score1 == score2) {
    		return true;
    	} else {
    		return false;
    	}
 
    }
    
    public double getOverallScore() {
    	
    	
  	  return (finalExam / FINAL_MAX) * FINAL_WEIGHT + (midtermExam / MIDTERM_MAX) * MIDTERM_WEIGHT;
    }
    
    public void getFinalLetterGrade() {
    	
    	double score;
    	
    	score = getOverallScore();
    }
}
