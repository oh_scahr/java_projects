/********************************************
* AUTHOR: Oscar Lopez
* COURSE:  CS 111 Intro to CS I
* SECTION:  M/W 11:05AM-1:05PM
* HOMEWORK #:  Homework 5, Project 2
********************************************/
/*****************************************************************************
* Exam Stats
*****************************************************************************
* PROGRAM DESCRIPTION:
* This program will give stats based on the exam scores the user enters.
*****************************************************************************
* ALGORITHM / PSEUDOCODE:
* 
* DECLARE: int number of a's, number of b's, number of c's, number of d's
*				  number of f's, total scores.
*
*			  double A percentage, B percentage, C percentage, D percentage,
*					 F percentage.
* 
* ASSIGN: numbers of a's = 0
*		  numbers of b's = 0
*		  numbers of c's = 0
*		  numbers of d's = 0
*		  numbers of f's = 0
*		  total scores = 0
* DO:
*	 PROMPT: Enter grade score out of 100.Enter -1 when done.
*	 INPUT: score = user input
*
* 	 IF score >= 90 THEN
*		numbers of a's + 1
*  		total scores + 1
*	 IF score >= 80 AND <90 THEN
*		numbers of b's + 1
*		total scores + 1
*	 IF score >= 70 AND <80 THEN
*		numbers of c's + 1
*		total scores + 1
*	 IF score >= 60 AND <70 THEN
*		numbers of d's + 1
*		total scores + 1
*	 IF score >= 0 AND <60 THEN
*		numbers of f's + 1
*		total scores + 1
*	
* WHILE score >= 0
*
* CALC A percentage = numbers of a's / total scores * 100
* 	   B percentage = numbers of b's / total scores * 100
* 	   C percentage = numbers of c's / total scores * 100
*	   D percentage = numbers of d's / total scores * 100
*	   F percentage = numbers of f's / total scores * 100
*
* PRINTLN: Total number of scores
* PRINTLN: numbers of a's and its percentage
* PRINTLN: numbers of b's and its percentage
* PRINTLN: numbers of c's and its percentage
* PRINTLN: numbers of d's and its percentage
* PRINTLN: numbers of f's and its percentage
*
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
* javax.swing.JOptionPane: For user input and output.
* java.text.DecimalFormat: To format percentages.
*****************************************************************************/
import javax.swing.JOptionPane;
import java.text.DecimalFormat;

public  class ExamStats 
{
	public static final DecimalFormat PERCENT_SPEC = new DecimalFormat("#0.#");
	
	public  static  void main(String[] args)
	{
	/***** DECLARATION SECTION *****/
	String score;
	
	int numberOfAs, numberOfBs, numberOfCs, numberOfDs, numberOfFs,
			totalScores;
	
	double percentageA, percentageB, percentageC, percentageD, percentageF;
	
	/***** INITIALIZATION SECTION *****/
			
	numberOfAs = 0;
	numberOfBs = 0;
	numberOfCs = 0;
	numberOfDs = 0;
	numberOfFs = 0;
	totalScores = 0;
	
	/***** INPUT SECTION *****/
	do
	{
		score = JOptionPane.showInputDialog("Enter grade score out of 100 \n"
										+ "Enter -1 to exit");
		
		if(Integer.parseInt(score) >= 90)
		{
			numberOfAs++;
			totalScores++;
		}
		
		if (Integer.parseInt(score) >= 80 && Integer.parseInt(score) < 90)
		{
			numberOfBs++;
			totalScores++;
		}
		
		if (Integer.parseInt(score) >= 70 && Integer.parseInt(score) < 80)
		{
			numberOfCs++;
			totalScores++;
		}
		
		if ( Integer.parseInt(score) >= 60 && Integer.parseInt(score) < 70)
		{
			numberOfDs++;
			totalScores++;
		}
		
		if (Integer.parseInt(score) < 60 && Integer.parseInt(score) >= 0)
		{
			numberOfFs++;
			totalScores++;
		}
	} while (Integer.parseInt(score) >= 0);
	
	/***** PROCESSING SECTION *****/
	
	percentageA = (numberOfAs / (double)totalScores) * 100;
	percentageB = (numberOfBs / (double)totalScores) * 100;
	percentageC = (numberOfCs / (double)totalScores) * 100;
	percentageD = (numberOfDs / (double)totalScores) * 100;
	percentageF = (numberOfFs / (double)totalScores) * 100;

	/***** OUTPUT SECTION *****/
	
	JOptionPane.showMessageDialog(null, "Total number of grades: " + 
								  totalScores + "\n" + "Number of A's: " +
								  numberOfAs + " ,which is " + 
								  PERCENT_SPEC.format(percentageA) + "%" +
								  "\n" + "Number of B's: " + numberOfBs + 
								  " ,which is "+ 
								  PERCENT_SPEC.format(percentageB) + "%" +
								  "\n" + "Number of C's: " + numberOfCs +
								  " ,which is " + 
								  PERCENT_SPEC.format(percentageC) + "%" +
								  "\n" + "Number of D's: " + numberOfDs + 
								  " ,which is " + 
								  PERCENT_SPEC.format(percentageD) + "%" +
								  "\n" + "Number of F's: " + numberOfFs +
								   " ,which is " + 
								   PERCENT_SPEC.format(percentageF) + "%"); 
								 
	System.exit(0);
	}
}