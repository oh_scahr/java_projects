/********************************************
* AUTHOR: Oscar Lopez
* COURSE:  CS 111 Intro to CS I
* SECTION:  M/W 11:05AM - 1:05PM
* HOMEWORK #:  Homework 5 Project 1
********************************************/
/*****************************************************************************
* Sentence Reactor
*****************************************************************************
* PROGRAM DESCRIPTION:
* This program will react based to a sentence the user enters.
*****************************************************************************
* ALGORITHM:
* 
* PROMPT: Type any sentence of your choice.
* INPUT: usrStr = user input
*
* IF usrStr ends with '?' AND length is even THEN
*	PRINT: Yes.
* ELSE IF usrStr ends with '?' AND length is odd THEN
*	PRINT: No.
* ELSE IF usrStr ends with '!'
*	PRINT: Wow.
* ELSE 
*	PRINT: You always say "usrStr"
* END IF
* 
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
* javax.swing.JOptionPane: For user input and display output.
*****************************************************************************/
import javax.swing.JOptionPane;

public  class SentenceReaction
{
	
	public  static  void main(String[] args)
	{
		/***** DECLARATION SECTION *****/
	
		int endsWith;
		String usrStr;
		char lastChar;
		Boolean isEven,isOdd;
	
		/***** INITIALIZATION SECTION *****/		
		/***** INPUT SECTION *****/
		
		usrStr = JOptionPane.showInputDialog("Type any sentence " +
											"of your choice.");
											
	
		/***** PROCESSING SECTION *****/	
		
		isEven = usrStr.length() % 2 == 0;
		
		isOdd = usrStr.length() % 2 != 0;
		
		endsWith = usrStr.charAt(usrStr.length() - 1);
		
		/***** OUTPUT SECTION *****/
		
		if(endsWith == '?' && isEven)
		{
			JOptionPane.showMessageDialog(null, "Yes.");
		}
		
		else if(endsWith == '?' && isOdd)
		{
			JOptionPane.showMessageDialog(null, "No.");
		}
		
		else if(endsWith == '!')
		{
			JOptionPane.showMessageDialog(null, "Wow.");
		}
		
		else 
		{
			JOptionPane.showMessageDialog(null, "You always say \n " + 
										  "\"" + usrStr + "\"");
		}
		
		System.exit(0);
}
}