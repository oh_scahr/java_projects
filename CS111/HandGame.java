public class HandGame 
{
    public static final int ROCK = 1;
    public static final int PAPER = 2;
    public static final int SCISSOR = 3;
    
    public static int getHandSign()
    {
    	int sign;
    	sign = (int)(Math.random() * 3) + 1;
    	return sign;
    }
    
    /****************************************************
     *Description: Will print out sign based on hand sign
     ****************************************************
     *Preconditions: Sign must be given.
     */
    
    public static void printHandSign(int sign)
    {
    	switch (sign)
    	{
    	
	    	case ROCK:
	    			 System.out.println("Rock");
	    	break;
	    	
	    	case PAPER: 
	    			System.out.println("Paper");
	    	break;
	    	
	    	case SCISSOR: 
	    			System.out.println("Scissor");
	    	break;
	    	
	    	default: System.out.println("ERROR!");
    	}
    }
}
