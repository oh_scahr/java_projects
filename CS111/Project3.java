/********************************************
* AUTHOR: 						  Oscar Lopez
* COURSE:  			  	 CS 111 Intro to CS I
* SECTION:  				 M/W 11:05-1:05pm
* HOMEWORK #:  			 Homework2, Project 3
********************************************/
/*****************************************************************************
* Farenheit to Celsius Converter
*****************************************************************************
* PROGRAM DESCRIPTION:
*
* This program will convert a farenhiet temperature to celsius temperature. 
*****************************************************************************
* ALGORITHM:
* 
* 1)Have the user enter a temperature in farenheit.
*
* 2)Use the following to calculate temperature:
*
*		Celsius = 5 (F-32) / 9
*
*	a)F = userInput.
*
* 3)Format result using DecimalFormat.
*
* 4)Output formatted result.
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
*
* java.util.Scanner : For user input of Farenheit value
* java.text.DecimalFormat : To format final result.
*****************************************************************************/

import java.util.Scanner;
import java.text.DecimalFormat;

public class Project3
{
	public static void main (String[] args)
	{
		String farenheit;
		double celsius;
		
		//User enters farenheit data.
		
		System.out.print("Enter the amount of degrees farenheit you would like to convert to celsius:");
		
		Scanner input = new Scanner(System.in);
		farenheit = input.nextLine();
		
		//Calculates farenheit to celsius.
		
		celsius = 5 * (Integer.parseInt(farenheit) -32) / 9.0;
	
		//Formats result to one decimal place.
		
		DecimalFormat celsiusFormat = new DecimalFormat("#0.0");
		
		//Outputs formatted celsius.
		System.out.println(farenheit + " degrees Farenheit = " + celsiusFormat.format(celsius) + " degrees Celsius.");
	}
}