public class Date {

    /*** CONSTANT VARIABLES ***/
    public static final String DEFAULT_MONTH = "December";
    public static final int DEFAULT_DAY = 31;
    public static final int DEFAULT_YEAR = 1960;
    
    /*** INSTANCE VARIABLES ***/
    private String month;
    private int day;
    private int year; //four digit year
    
    /*** CONSTRUCTORS ***/
    public Date()
    {
        boolean isValid;
        isValid = setDate(DEFAULT_MONTH, DEFAULT_DAY, DEFAULT_YEAR);
        
        if(!isValid)
        {
            System.out.println("ERROR: bad Date in default Date()");
            System.exit(0);
        }
    }
    
    public Date(String month)
    {
        boolean isValid;
        isValid = setDate(month, DEFAULT_DAY, DEFAULT_YEAR);
        
        if(!isValid)
        {
            System.out.println("ERROR: bad Date in Date(String)");
            System.exit(0);
        }
    }
    
    public Date(String month, int day, int year)
    {
        boolean isValid;
        isValid = setDate(month, day, year);
        
        if(!isValid)
        {
            System.out.println("ERROR: bad Date in full Date()");
            System.exit(0);
        }
    }
    
    /*** SETTERS / MUTATORS ***/
    
    // DESCRIPTION:        Sets all of Date's data
    // PRE-CONDITIONS:    All instance variables have valid values
    // POST-CONDITIONS:    Sets instance variables to parameter values if valid
        // and returns true. otherwise, does not change instance variables
        // and returns false
    public boolean setDate(String month, int day, int year) {
        boolean isValid;
        
        isValid = isValidMonth(month) && isValidDay(day) && isValidYear(year);
      
      	if(isValid)
      	{
      		setMonth(month);
      		setDay(day);
      		setYear(year);
      	}
       		return isValid;
       } 
    
        
   
    
    
    public boolean setMonth(String month)
    {
        boolean isValid;
        
        isValid = isValidMonth(month);
        
        if(isValid)
        {
            this.month = month;
        }
        
        return isValid;
    }
    
    public boolean setDay(int day)
    {
    	boolean isValid;
    	
    	isValid = isValidDay(day);
    	
    	if(isValid)
    	{
    		this.day = day;
    	}
    	
    	return isValid;
    }
    
    public boolean setYear(int year)
    {
    	boolean isValid;
    	
    	isValid = isValidYear(year);
    	
    	if(isValid)
    	{
    		this.year = year;
    	}
    	
    	return isValid;
    }

    
    /*** GETTERS / ACCESSORS ***/
    public String getMonth()
    {
        return this.month;
    }
    
    public int getDay()
    {
    	return this.day;
    }
	
	public int getYear()
	{
		return this.year;
	}
	
	//post-condition: returns null if not a valid month
	public String monthToString(int month)
	{
		String result;
		
		switch(month)
		{
			case 1:
				result = "January";
				break;
				
			case 2:
				result = "February";
				break;
				
			case 3:
				result = "March";
				break;
				
			case 4:
				result = "April";
				break;
				
			case 5:
				result = "May";
				break;
				
			case 6:
				result = "June";
				break;
			
			default:
				result = null;
				break;
		}
		 Integer.toString(month);
	}
    
    /*** OTHER REQUIRED METHODS ***/ //toString, equals, etc.
    //description: returns String representation of Date
    public String toString()
    {
        int monthNum;
        monthNum = this.monthToInt(this.month);
        
        return monthNum + "/" + this.day + "/" + this.year;
    }
    
    public boolean equals(Date otherDate)
    {
        return this.month.equals(otherDate.month) && this.day == otherDate.day
                && this.year == otherDate.year;
    }
    
    
    
    /*** HELPER METHODS ***/ //isValid...
    
    
    // DESCRIPTION:        Print date in MM DD, YYYY format
    // PRE-CONDITIONS:    All instance variables have value
    // POST-CONDITIONS:    Date output to console
    public void writeOutput() {
        System.out.println( this.toString() );
    }
    
    
    private boolean isValidMonth(String month)
    {
        int monthNum;
        monthNum = this.monthToInt(month);

        return monthNum >=1 && monthNum <= 12;
    }
    
    
    private boolean isValidDay(int day)
    {
    	return day >=1 && day <=31;
    }
   
    private boolean isValidYear(int year)
    {
    	return year >= 1000 && year <= 3000;
    }
    
    
   
   
    
    //post-condition: returns integer 1-12 for valid month, 0 for invalid
    private int monthToInt(String month)
    {
        int result;
        
        
        if( month.equalsIgnoreCase("January") )
        {
            result = 1;
        }
        else if( month.equalsIgnoreCase("February") )
        {
            result = 2;
        }
        else if( month.equalsIgnoreCase("March") )
        {
            result = 3;
        }
        else if( month.equalsIgnoreCase("April") )
        {
            result = 4;
        }
        else if( month.equalsIgnoreCase("May") )
        {
            result = 5;
        }
        else if( month.equalsIgnoreCase("June") )
        {
            result = 6;
        }
        else if( month.equalsIgnoreCase("July") )
        {
            result = 7;
        }
        else if( month.equalsIgnoreCase("August") )
        {
            result = 8;
        }
        else if( month.equalsIgnoreCase("September") )
        {
            result = 9;
        }
        else if( month.equalsIgnoreCase("October") )
        {
            result = 10;
        }
        else if( month.equalsIgnoreCase("November") )
        {
            result = 11;
        }
        else if( month.equalsIgnoreCase("December") )
        {
            result = 12;
        }
        else
        {
            result = 0;
        }
        
        
        return result;
    }
    

    
    
    
    
    
    
    
    
    
    
    
    

    
}