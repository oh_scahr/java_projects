public class Counter
{
	private int count;
	
	public String toString(int count1)
	{
		return Integer.toString(count1);
	}
	
	public void reset()
	{
		count = 0;
	}
	
	public int getCount()
	{
		return count;
	}
	
	public void add()
	{
		count++;
	}
	
	public void minus()
	{
		//Assures count will not be negative.
		if(count > 0)
		{
			count--;
		}
	}
	
	public boolean equals( String count1, String count2)
	{
		return count1.equals(count2);
	}
}