import java.util.Scanner;

public class TemperatureTester 
{
    
    public static void main(String[] args)
    {
    	
    	Scanner input;
    	int temperature;
    	String temp;
    	Temperature myTemperature;
    	
    	input = new Scanner(System.in);
    	myTemperature = new Temperature();
    	
        System.out.println("Enter a temperature followed by f for fahrenheit"
        					+ " or c for celsius:");
        
        temp = input.nextLine();
        
        temperature = Integer.parseInt(temp.substring(temp.length() - 1));
        
        if(temp.charAt(temp.length()) = 'f' || 
           temp.charAt(temp.length()) = 'F')
        {
        	myTemperature.setScale('F');
        }
        
        else if (temp.charAt(temp.length()) = 'c' || 
        		 temp.charAt(temp.length()) = 'C')
        {
        	myTemperature.setScale('C');
        }
        
        else
        {
        	myTemperature();
        }
        
        
    }
}
