/********************************************
* AUTHOR: Oscar Lopez			
* COURSE:  CS 111 Intro to CS I
* SECTION:  M/W 11:10AM - 1:05PM
* HOMEWORK #:  Homework 4, Project 1
********************************************/
/*****************************************************************************
* Mr.Headsor Tails
*****************************************************************************
* PROGRAM DESCRIPTION:
* This program will calculate percentages based on a game of heads or tails 
* with 8 tosses.
*****************************************************************************
* ALGORITHM:
*
* PRINT: Enter 'H' for heads or 'T' for tails for each coin toss.
* PROMPT: First Toss:
* INPUT: Input = toss1
* IF toss1 == 'H' or 'h' THEN
*	headCount ++
* ELSE 
*	tailsCount ++
* ENDIF
* coinTosses ++
* 
* PROMPT: Second Toss:
* INPUT: Input = toss2
* IF toss2 == 'H' or 'h' THEN
*	headCount ++
* ELSE 
*	tailsCount ++
* ENDIF
* coinTosses ++
* 
* PROMPT: Third Toss:
* INPUT: Input = toss3
* IF toss3 == 'H' or 'h' THEN
*	headCount ++
* ELSE 
*	tailsCount ++
* ENDIF
* coinTosses ++
*
* PROMPT: Fourth Toss:
* INPUT: Input = toss4
* IF toss4 == 'H' or 'h' THEN
*	headCount ++
* ELSE 
*	tailsCount ++
* ENDIF
* coinTosses ++
*
* PROMPT: Fifth Toss:
* INPUT: Input = toss5
* IF toss5 == 'H' or 'h' THEN
*	headCount ++
* ELSE 
*	tailsCount ++
* ENDIF
* coinTosses ++
*
* PROMPT: Sixth Toss:
* INPUT: Input = toss6
* IF toss6 == 'H' or 'h' THEN
*	headCount ++
* ELSE 
*	tailsCount ++
* ENDIF
* coinTosses ++
*
* PROMPT: Seventh Toss:
* INPUT: Input = toss7
* IF toss7 == 'H' or 'h' THEN
*	headCount ++
* ELSE 
*	tailsCount ++
* ENDIF
* coinTosses ++
*
* PROMPT: Eighth Toss:
* INPUT: Input = toss8
* IF toss8 == 'H' or 'h' THEN
*	headCount ++
* ELSE 
*	tailsCount ++
* ENDIF
* coinTosses ++
*
* CALC: number of heads = headCount
* CALC: number of tails = tailsCount
* CALC: heads percentage = headCount/ coinTosses
* CALC: tails percentage = tailsCount/coinTosses
*
* PRINT: Number of heads: + headCount
* PRINT: Number of tails: + tailsCount
* PRINT: Heads Percent: + heads percentage
* PRINT: Tails Percent: + tails percentage
* 
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
*
* javax.swing.JOptionPane - used to prompt user for input and display result.
* java.text.DecimalFormat - formats percentages.
*****************************************************************************/
import javax.swing.JOptionPane;
import java.text.DecimalFormat;

public  class HeadsOrTails
{
	public static final DecimalFormat PERCENT_SPEC = new DecimalFormat("#0.0#");

	public  static  void main(String[] args)
	{
		
		/***** DECLARATION SECTION *****/
		
		int headCount,tailsCount,coinTosses,toss1,toss2,toss3,toss4,toss5,
		    toss6,toss7,toss8;
		
		String input1,input2,input3,input4,input5,input6,input7,input8;
			
		double headsPercent,tailsPercent;
		
	
		
		/***** INITIALIZATION SECTION *****/
		
		headCount = 0;
		tailsCount = 0;
		coinTosses = 0; 
		
		/***** INPUT SECTION *****/
		
		input1 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input2 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input3 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input4 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input5 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input6 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input7 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input8 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		
		/***** PROCESSING SECTION *****/
		
		toss1 = input1.charAt(0);

		if(toss1 == 'H' || toss1 == 'h')
		{
			headCount++;
		}
		else
		{
			tailsCount++;
		}
		
		coinTosses++;
		
		toss2 = input2.charAt(0);
		
		if(toss2 == 'H' || toss2 == 'h')
		{
			headCount++;
		}
		else
		{
			tailsCount++;
		}
		
		coinTosses++;
		
		toss3 = input3.charAt(0);
		
		if(toss3 == 'H' || toss3 == 'h')
		{
			headCount++;
		}
		else
		{
			tailsCount++;
		}
		
		coinTosses++;
		
		toss4 = input4.charAt(0);
		
		if(toss4 == 'H' || toss4 == 'h')
		{
			headCount++;
		}
		else
		{
			tailsCount++;
		}
		
		coinTosses++;
		
		toss5 = input5.charAt(0);
		
		if(toss5 == 'H' || toss5 == 'h')
		{
			headCount++;
		}
		else
		{
			tailsCount++;
		}
		
		coinTosses++;
		
		toss6 = input6.charAt(0);
		
		if(toss6 == 'H' || toss6 == 'h')
		{
			headCount++;
		}
		else
		{
			tailsCount++;
		}
		
		coinTosses++;
		
		toss7 = input7.charAt(0);
		
		if(toss7 == 'H' || toss7 == 'h')
		{
			headCount++;
		}
		else
		{
			tailsCount++;
		}
		
		coinTosses++;
		
		toss8 = input8.charAt(0);
		
		if(toss8 == 'H' || toss8 == 'h')
		{
			headCount++;
		}
		else
		{
			tailsCount++;
		}
		
		coinTosses++;
		
		
		headsPercent = ((double)headCount / coinTosses) * 100;
		tailsPercent = ((double)tailsCount / coinTosses) * 100;
		
		
		/***** OUTPUT SECTION *****/
		
		JOptionPane.showMessageDialog(null, "Heads: " + headCount + " Tails: " 
									  + tailsCount + " Heads Percent: " +
									  PERCENT_SPEC.format(headsPercent) + 
									  "%" +" Tails Percent: " + 
									  PERCENT_SPEC.format(tailsPercent)
									  + "%");
		
	}
}