/********************************************
* AUTHOR: Oscar Lopez			
* COURSE:  CS 111 Intro to CS I
* SECTION:  M/W 11:10AM - 1:05PM
* HOMEWORK #:  Homework 4, Project 1
********************************************/
/*****************************************************************************
* Mr.Headsor Tails 2
*****************************************************************************
* PROGRAM DESCRIPTION:
* This program will calculate percentages based on a game of heads or tails 
* with 8 tosses.
*****************************************************************************
* ALGORITHM:
* 'H' = 72 in ASCII
* 'T' = 84 in ASCII
*
* PROMPT: Enter H or T for heads/tails.
*
* PROMPT: First toss;
* INPUT: H or T
* INPUT 1 = INPUT
* CALC: coinToss++
* CALC: sum += INPUT 1
*
* PROMPT: Second toss;
* INPUT: H or T
* INPUT 2 = INPUT
* CALC: coinToss++
* CALC: sum += INPUT 2
* 
* PROMPT: Third toss;
* INPUT: H or T
* INPUT 3 = INPUT
* CALC: coinToss++
* CALC: sum += INPUT 3
*
* PROMPT: Fourth toss;
* INPUT: H or T
* INPUT 4 = INPUT
* CALC: coinToss++
* CALC: sum += INPUT 4
*
* PROMPT: Fifth toss;
* INPUT: H or T
* INPUT 5 = INPUT
* CALC: coinToss++
* CALC: sum += INPUT 5
*
* PROMPT: Sixth toss;
* INPUT: H or T
* INPUT 6 = INPUT
* CALC: coinToss++
* CALC: sum += INPUT 6
*
* PROMPT: Seventh toss;
* INPUT: H or T
* INPUT 7 = INPUT
* CALC: coinToss++
* CALC: sum += INPUT 7
*
* PROMPT: Eighth toss;
* INPUT: H or T
* INPUT 8 = INPUT
* CALC: coinToss++
* CALC: sum += INPUT 8
*
*
*
* CALC: number of heads = sum / 'H'
* CALC: number of tails = sum / 'T'
*
* CALC: heads percentage = headCount/ coinTosses
* CALC: tails percentage = tailsCount/coinTosses
*
* PRINT: Number of heads: + headCount
* PRINT: Number of tails: + tailsCount
* PRINT: Heads Percent: + heads percentage
* PRINT: Tails Percent: + tails percentage
* 
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
*
* javax.swing.JOptionPane - used to prompt user for input and display result.
* java.text.DecimalFormat - formats percentages.
*****************************************************************************/
import javax.swing.JOptionPane;
import java.text.DecimalFormat;

public  class HeadsOrTails2
{
	public static final DecimalFormat PERCENT_SPEC = new DecimalFormat("#0.0#");

	public  static  void main(String[] args)
	{
		
		/***** DECLARATION SECTION *****/
		
		int headCount,tailsCount,coinTosses,toss1,toss2,toss3,toss4,toss5,
		    toss6,toss7,toss8;
		
		String input1,input2,input3,input4,input5,input6,input7,input8;
			
		double headsPercent,tailsPercent;
		
	
		
		/***** INITIALIZATION SECTION *****/
		headCount = 0;
		tailsCount = 0;
		coinTosses = 0; 
		
		/***** INPUT SECTION *****/
		
		input1 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input2 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input3 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input4 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input5 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input6 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input7 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		input8 = JOptionPane.showInputDialog("Enter 'H' for heads.'T' for Tails");
		
		/***** PROCESSING SECTION *****/
		
		toss1 = input1.charAt(0);
		headCount += toss1 %'H';
		tailsCount += toss1 %'T';
		coinTosses++;
		
		toss2 = input2.charAt(0);
		headCount += toss2 %'H';
		tailsCount += toss2 %'T';
		coinTosses++;
		
        toss3 = input3.charAt(0);
        headCount += toss3 %'H';
		tailsCount += toss3 %'T';
        coinTosses++;
        
        toss4 = input4.charAt(0);
        headCount += toss4 %'H';
		tailsCount += toss4 %'T';
        coinTosses++;
        
        toss5 = input5.charAt(0);
        headCount += toss5 %'H';
		tailsCount += toss5 %'T';
        coinTosses++;
        
        toss6 = input6.charAt(0);
        headCount += toss6 %'H';
		tailsCount += toss6 %'T';
        coinTosses++;
        
        toss7 = input7.charAt(0);
        headCount += toss7 %'H';
		tailsCount += toss7 %'T';
        coinTosses++;
        
        toss8 = input8.charAt(0);
        headCount += toss8 %'H';
		tailsCount += toss8 %'T';
        coinTosses++;
        
		headsPercent = (headCount / (double)coinTosses) * 100;
		tailsPercent = (tailsCount / (double)coinTosses) * 100;
		
		
		/***** OUTPUT SECTION *****/
		
		JOptionPane.showMessageDialog(null, "Heads: " + headCount + " Tails: " 
									  + tailsCount + " Heads Percent: " +
									  PERCENT_SPEC.format(headsPercent) + 
									  "%" +" Tails Percent: " + 
									  PERCENT_SPEC.format(tailsPercent)
									  + "%");
		
	}
}