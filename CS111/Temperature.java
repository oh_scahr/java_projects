
public class Temperature 
{
	//CONSTANTS
	
	public static final double DEFAULT_TEMPERATURE = 0.0;
	public static final char DEFAULT_SCALE = 'C';
	
	//INSTANCE VARIABLES
	
	private double temperature;
	private char scale;
	
	//CONSTRUCTOR
	
	/*********************************************************************
	 *Description: Instantiates temperature and scale to their default value.
	 *********************************************************************
	 *Preconditions: Instance variables are declared.
	 *********************************************************************
	 *Postconditions: Instantiates temperature and scale to default values.
	 ********************************************************************/
	 
	 public Temperature()
	 {
	 	boolean isValid;
	 	
	 	isValid = setBoth(DEFAULT_TEMPERATURE, DEFAULT_SCALE);
	 	
	 	if(!isValid)
	 	{
	 		System.out.println("ERROR: Default values are invalid!");
	 		System.exit(0);
	 	}
	 }
	 
	 
	 
	/*********************************************************************
	 *Description: Instantiates temperature and scale to given values
	 *********************************************************************
	 *Preconditions: Instance variables are declared.
	 *********************************************************************
	 *Postconditions: Instantiates temperature and scale to given values.
	 ********************************************************************/
	 
	 public Temperature(double temperature, char temperatureScale)
	 {
	 	boolean isValid;
	 	
	 	isValid = setBoth(temperature, temperatureScale);
	 	
	 	if(!isValid)
	 	{
	 		System.out.println("ERROR: Given values are invalid!");
	 		System.exit(0);
	 	}
	 }
	 
	 /*********************************************************************
	 *Description: Instantiates temperature to valid value and defaults scale.
	 *********************************************************************
	 *Preconditions: Instance variables are declared.
	 *********************************************************************
	 *Postconditions: Instantiates temperature to valid value.
	 *				  Defaults scale to default value.
	 ********************************************************************/
	 
	 public Temperature(double temperature)
	 {
	 	boolean isValid;
	 	
	 	isValid = setTemperature(temperature);
	 	setScale(DEFAULT_SCALE);
	 	
	 	if(!isValid)
	 	{
	 		System.out.println("ERROR: Given temperature is invalid!");
	 		System.exit(0);
	 	}
	 }
	 
	 /*********************************************************************
	 *Description: Instantiates scale to given value and defaults temperature.
	 *********************************************************************
	 *Preconditions: Instance variables are declared.
	 *********************************************************************
	 *Postconditions: Instantiates the instance variables to given values.
	 *				  Defaults temp to default value.
	 ********************************************************************/
	 
	 public Temperature(char scale)
	 {
	 	boolean isValid;
	 	
	 	isValid = setScale(scale);
	 	setTemperature(DEFAULT_TEMPERATURE);
	 	
	 	if(!isValid)
	 	{
	 		System.out.println("ERROR: Given scale value is invalid!");
	 		System.exit(0);
	 	}
	 }
	 
	 //ACCESSORS(GETTERS)
	 
	 
	 /*********************************************************************
	 *Description: Gets converted fahrenheit value.
	 *********************************************************************
	 *Preconditions: Instance variables are declared. Given value is of
	 *				 celsius scale.
	 *********************************************************************
	 *Postconditions: Returns converted celsius value to fahrenheit.
	 ********************************************************************/
	 
	 public double getTemperatureFahrenheit()
	 {
	 	double fahrenheit;
	 	
	 	fahrenheit = (1.8 * this.temperature) + 32.0;
	 	setTemperature(fahrenheit);
	 	
	 	return this.temperature;
	 }
	 
	 /*********************************************************************
	 *Description: Gets converted Celsius value.
	 *********************************************************************
	 *Preconditions: Instance variables are declared. Given value is of
	 *				 fahrenheit scale.
	 *********************************************************************
	 *Postconditions: Returns converted fahrenheit value to celsius.
	 ********************************************************************/
	 
	 public double getTemperatureCelsius()
	 {
	 	double celsius;
	 	
	 	celsius = (this.temperature - 32.0) / 1.8;
	 	setTemperature(celsius);
	 	
	 	return this.temperature;
	 }
	 
	 //MUTATORS(SETTERS)
	 
	 /*********************************************************************
	 *Description: Sets scale.
	 *********************************************************************
	 *Preconditions: Instance variables are declared. Valid scale value is
	 *	   			 given.
	 *********************************************************************
	 *Postconditions: Sets valid value to scale.
	 ********************************************************************/
	 
	 public boolean setScale(char scale)
	 {
	 	boolean isValid;
	 	
	 	isValid = isValidScale(scale);
	 	
	 	if(isValid)
	 	{
	 		this.scale = scale;
	 	}
	 	
	 	return isValid;
	 }
	 
	 /*********************************************************************
	 *Description: Sets temperature.
	 *********************************************************************
	 *Preconditions: Instance variables are declared. Valid temperature is 
	 *				 given.
	 *********************************************************************
	 *Postconditions: Sets temperature value to temperature.
	 ********************************************************************/
	 
	 public boolean setTemperature(double temperature)
	 {
	 	boolean isValid;
	 	
	 	isValid = isValidTemperature(temperature);
	 	
	 	if(isValid)
	 	{
	 		this.temperature = temperature;
	 	}
	 	
	 	return isValid;
	 }
	 
	 /*********************************************************************
	 *Description: Sets both temperature and scale.
	 *********************************************************************
	 *Preconditions: Instance variables are declared. Both temperature and 
	 *				 scale are valid and given.
	 *********************************************************************
	 *Postconditions: Sets values to temperature and scale.
	 ********************************************************************/
	 
	 public boolean setBoth(double temperature, char scale)
	 {
	 	boolean isValid;
	 	
	 	isValid = isValidTemperature(temperature) && isValidScale(scale);
	 	
	 	if(isValid)
	 	{
	 		setTemperature(temperature);
	 		setScale(scale);
	 	}
	 	
	 	return isValid;
	 }
	 
	 /*********************************************************************
	 *Description: Checks equality.
	 *********************************************************************
	 *Preconditions: Instance variables are declared. Two temperature values
	 *				 are given. 
	 *********************************************************************
	 *Postconditions: returns validity(true/false).
	 ********************************************************************/
	 
	 public boolean  equals(Temperature otherTemp)
	 {
	 	return this.temperature == otherTemp.temperature;
	 }
	 
	 /*********************************************************************
	 *Description: Compares temperatures for size.
	 *********************************************************************
	 *Preconditions: Instance variables are declared. Valid temperature values
	 *				 are given.
	 *********************************************************************
	 *Postconditions: returns true or false.
	 ********************************************************************/
	 
	 public boolean isGreater(Temperature otherTemp)
	 {
	 	return this.temperature > otherTemp.temperature;
	 }
	 
	 public boolean isLess(Temperature otherTemp)
	 {
	 	return this.temperature < otherTemp.temperature;
	 }
	 
	 /*********************************************************************
	 *Description: Returns String intepretation of results.
	 *********************************************************************
	 *Preconditions: Instance variables are declared.
	 *********************************************************************
	 *Postconditions: Returns string.
	 ********************************************************************/
	 
	 public String toString()
	 {
	 	return temperature + " " + scale;
	 }
	 
	 /*********************************************************************
	 *Description: Checks validity tmperature.
	 *********************************************************************
	 *Preconditions: Instance variables are declared. Temperature is given.
	 *********************************************************************
	 *Postconditions: Returns true or false.
	 ********************************************************************/
	 
	 public boolean isValidTemperature(double temperature)
	 {
	 	return temperature >= -459.67 && temperature <= 212.00;
	 }
	 
	 /*********************************************************************
	 *Description: Checks valid scale.
	 *********************************************************************
	 *Preconditions: Instance variables are declared. Scale is given.
	 *********************************************************************
	 *Postconditions: Returns true or false.
	 ********************************************************************/
	 
	 public boolean isValidScale(char scale)
	 {
	 	return scale == 'C' || scale == 'F';
	 }
	 
}
