import java.util.Scanner;

class HandGameTester {
	public static void main(String[] args)
	{
		//DECLERATION
		Scanner keyboard;
		int userSign;
		int compSign;
		
		//INIIALIZE
		keyboard = new Scanner(System.in);
		
		//INPUT
		userSign = Cs111.readInt("Enter hand sign"
								  + " (1 for Rock, 2 for Paper, 3 for Scissors): "
								  ,keyboard,1,3);
		
		compSign = HandGame.getHandSign();
		
		//PROCESSING
		System.out.println("Your hand was ");
		HandGame.printHandSign(userSign);
		System.out.println("Computer's hand was "); 
		HandGame.printHandSign(compSign);
		
		if(userSign == compSign)
		{
			System.out.println("Tie");
		}
		else if (userSign == HandGame.ROCK && compSign == HandGame.PAPER)
		{
			System.out.println("You Lose :(");
		}
		else if(userSign == HandGame.ROCK && compSign == HandGame.SCISSOR)
		{
			System.out.println("You win! :D");
		}
		else if(userSign == HandGame.PAPER && compSign == HandGame.ROCK)
		{
			System.out.println("You win! :D");
		}
		else if(userSign == HandGame.PAPER && compSign == HandGame.SCISSOR)
		{
			System.out.println("You Lose :(");
		}
		else if(userSign == HandGame.SCISSOR && compSign == HandGame.ROCK)
		{
			System.out.println("You Lose :(");
		}
		else if (userSign == HandGame.SCISSOR && compSign == HandGame.PAPER)
		{
			System.out.println("You win! :D");
		}
		
	}
}
