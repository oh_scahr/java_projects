
public class StudentRecord 
{
    
    public static final int  FINAL_MAX = 100;
    public static final int MIDTERM_MAX = 100;
    public static final int FINAL_MIN = 0;
    public static final int MIDTERM_MIN = 0 ;
    public static final double FINAL_WEIGHT =.65;
    public static final double MIDTERM_WEIGHT = .35;
    public static final int QUIZ_MAX = 30;
    public static final int QUIZ_MIN = 0;
    public static final double QUIZ_WEIGHT = 0.25;
    
    //DEFAULTS
    
    public static final int QUIZ_DEFAULT = 0;
    public static final int EXAM_DEFAULT = 0;
    
    
    private int finalExam = 0;
    private int midtermExam = 0;
    private int quizScore = 0;

 
    
    //CONSTRUCTOR
    
    public void StudentRecord(int quiz1, int quiz2, int quiz3, int midtermScore,
    						  int finalScore)
    {
    	this.setQuizScore(quiz1);
    	this.setQuizScore(quiz2);
    	this.setQuizScore(quiz3);
    	this.setMidtermExam(midtermScore);
    	this.setFinalExam(finalScore);	
    	this.getOverallScore();
    	this.toString();
    }
    
    //DEFAULT CONSTRUCTOR
    
    public void StudentRecord()
    {
    	this.setQuizScore(QUIZ_DEFAULT);
    	this.setMidtermExam(EXAM_DEFAULT);
    	this.setFinalExam(EXAM_DEFAULT);
    	this.getOverallScore();
    	this.toString();
    }
    						  
    
    
    //SETTERS(MUTATORS)
    
    private void setFinalExam(int score) 
    {
    	this.finalExam = score;
    }
    
    private void setMidtermExam(int score) 
    {
    	this.midtermExam = score;
    }
    
    private void setQuizScore(int score)
    {
    	this.quizScore = this.quizScore + score;
    }
    
   
    
    //GETTERS(ACCESSORS)
    
    public int getFinalExam()
    {
    	return finalExam;
    }
    
    public int getMidtermExam() 
    {
    	return midtermExam;
    }
    
    public int getQuizScore()
    {
    	return quizScore;
    }
    
    public double getOverallScore()
    {
    		double overallScore =  (finalExam / FINAL_MAX) * FINAL_WEIGHT + 
  	  		 		  		(midtermExam / MIDTERM_MAX) * MIDTERM_WEIGHT +
  	  				  		(quizScore / QUIZ_MAX) * QUIZ_WEIGHT;
  	  				  		
  	  		return overallScore;
  	  				  	
    }
    
    public char getFinalLetterGrade(double overallScore) 
    {
    	char grade = 0;
    	
    	if(overallScore >= 90)
    	{
    		grade = 'A';
    	}
    	else if(overallScore >= 80 && overallScore < 90)
    	{
    		grade = 'B';
    	}
    	
    	else if(overallScore >= 70 && overallScore < 80)
    	{
    		grade = 'C';
    	}
    	
    	else if(overallScore>= 60 && overallScore < 70)
    	{
    		grade = 'D';
    	}
    	
    	else if(overallScore < 60)
    	{
    		grade = 'F';
    	}
    	
    	return grade;
    }
    
    public String toString() 
    {
    	return "Your overall score is" + this.getOverallScore()+ "/n" + "/n" + 
    		   "Your letter grade: " + this.getFinalLetterGrade(this.getOverallScore());
    			
    }
    
    //HELPERS
    
    public boolean equals(int score1, int score2)
    {
    	return score1 == score2;
    }
    
    public boolean quizIsValid(int score)
    {
    	return (score >= QUIZ_MIN && score <= QUIZ_MAX);	
    }
    
    public boolean examIsValid(int score)
    {	
    	return (score >= FINAL_MIN && score <= FINAL_MAX);	
    }
    
   
}
