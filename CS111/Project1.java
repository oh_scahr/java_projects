/********************************************
* AUTHOR: 						Oscar Lopez		
* COURSE:  			   CS 111 Intro to CS I
* SECTION: 				   M/W 11:05-1:05pm.
* HOMEWORK #:  		   Homework#2 Project 1
* LAST MODIFIED: 		    January 30,2015
********************************************/
/*****************************************************************************
* Three Number Average
*****************************************************************************
* PROGRAM DESCRIPTION:
*
* This program will ask for three numbers from the user and will calculate the
* average.
*****************************************************************************
* ALGORITHM:
*
* 1)Have the user enter three numbers.
* 2)Average = (first number + second number + third number)/3.
* 3)Format the average to round to most 3 decimal places.
* 4)Print out the average.
*
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
*
* java.util.Scanner : To read user input for the three numbers.
* java.text.DecimalFormat : To format the answer to a rounded decimal answer.
*****************************************************************************/


import java.text.DecimalFormat;
import java.util.Scanner;

public class Project1
{
	public static void main(String[] args)
	{
		String firstNumber,secondNumber,thirdNumber;

		//This will take user input and use that input for the three numbers the program will average.
		
		System.out.println("Please enter three numbers.Press Enter after each number:");
		Scanner numbers = new Scanner(System.in);
		firstNumber = numbers.nextLine();
		secondNumber = numbers.nextLine();
		thirdNumber = numbers.nextLine();
		
		//Code that will calculate the average.
		
		double average;
		 
		average = (Integer.parseInt(firstNumber) + Integer.parseInt(secondNumber) + Integer.parseInt(thirdNumber))/3.0;
	
		
		//Outputs the average to the console and formats the result.
		
		DecimalFormat averageFormat = new DecimalFormat("#0.0##");
		
		System.out.println("The average of " + firstNumber + " , " + secondNumber
		 + " , and " + thirdNumber + " is " + averageFormat.format(average));
	
	}
	
}