import java.util.Scanner;

public class Topic11MoreMethods
{
    public static void main(String[] args)
    {

    }

	//STEP 0 = sit next to your (new) partner for today, fill out sign-in sheet
	//STEP 1 = Complete Cs111.java file
		// 2 pair projects making methods: readDouble(), readChar()
			// pseudocode both first, deskcheck, confirm with me, THEN code both
			// methods should include error-checking
	//STEP 2 = Compare Cs111.java files
		// each member compares to partners code,
			//offer suggestions by adding comments ONLY.
		// each pair fixes up their code according to suggestions.
	//STEP 3 = Group Project
		// Each member in pair writes pseudocode for same program:
			// Write a program that prints out the following menu:
				// Welcome User!
				// Main Menu
				// A: Tell me a joke
				// B: Tell me average of int and double
				// X: Exit program
				//
				// Enter choice:
				//
			// This is what should happen for each user choice:
				// Choice A: output a joke to the console
				// Choice B: prompt user for an integer 0-100, then a double 0-50.
				// 		output the average of the two numbers
				// Choice X: Print exit message and end the program
			// After user enters choice and action happens, menu should be
			//		output again and prompt user for choice again. Program
			//		should run until user enters X for choice then end.
		// Swap psuedocode with partner
		// Write Java code based off of THEIR pseudocode, NOT yours!
		// Does it work? make notes/suggestions on their paper on how to clarify
		// Get code to run based off of their solution
}