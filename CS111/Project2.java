/********************************************
* AUTHOR: 						  Oscar Lopez
* COURSE:  		   		 CS 111 Intro to CS I
* SECTION:  				 M/W 11:05-1:05pm
* HOMEWORK #:           Homework 2, Project 2
********************************************/
/*****************************************************************************
* Calorie Burning Program
*****************************************************************************
* PROGRAM DESCRIPTION:
*
* This program will calculate how many total calories will a person burn with 
* 30 mins of running, 30 mins of Basketball, and 6 hours of sleeping.
*****************************************************************************
* ALGORITHM:
*
* 1)State constants:
*		a)1 kilogram = 2.2 pounds.
* 		b)METS running = 10.
*		c)METS basketball = 8.
*		d)METS Sleeping = 1.
*
* 2)Have the user enter a weight in pounds.
*
* 3)Calculate the following for each activity.:
*
*		calories burned = 0.0175 * METS * (weight * 2.2) * mins.
* 
* 4)Add all calories from each calculated activity and store them to a variable
*   "total".
* 
* 5)Format total to a rounded decimal value.
* 
* 6)Output formatted total.
* 		
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
*
* java.util.Scanner : For user to imput weight value.
* java.text.DecimalFormat : For rounding the total value.
*****************************************************************************/

import java.util.Scanner;
import java.text.DecimalFormat;

public class Project2
{
	//Constants placed as finals.
	public static final double KILOGRAMS_TO_POUNDS_CONVERSION = 2.2;
	
	public static final int METS_RUNNING_6MPH = 10;
	public static final int METS_BASKETBALL = 8;
	public static final int METS_SLEEPING = 1;
	
	public static void main(String[] args)
	{
			
		String weight;	
		
		double runningBurnedCalories, basketballBurnedCalories, sleepingBurnedCalories;
			
		System.out.println("Enter any weight value in pounds:");
		
		//User enters weight.
		
		Scanner input = new Scanner(System.in);
		 
		weight = input.nextLine();
	
		//This calculates the number of calories burned for each activity.
		
			//Running 6MPH for 30 mins:
		runningBurnedCalories = (0.0175 * METS_RUNNING_6MPH * (Integer.parseInt(weight)/KILOGRAMS_TO_POUNDS_CONVERSION))*30; 			
			//Playing Basketball for 30 mins:
		basketballBurnedCalories = (0.0175 * METS_BASKETBALL * (Integer.parseInt(weight)/KILOGRAMS_TO_POUNDS_CONVERSION))*30;		
			//Sleeping for 6 hours(180mins):
		sleepingBurnedCalories = (0.0175 * METS_SLEEPING * (Integer.parseInt(weight)/KILOGRAMS_TO_POUNDS_CONVERSION))*180;
		
		//Total calories burned.
		double totalCaloriesBurned = runningBurnedCalories + basketballBurnedCalories + sleepingBurnedCalories;
		
		//Output and formats results.
		
		DecimalFormat totalFormat = new DecimalFormat("#0.0");
		
		System.out.println("A person weighing " + weight + " pounds will burn roughly " + totalFormat.format(totalCaloriesBurned) + " total calories.");

		
		
	}
}