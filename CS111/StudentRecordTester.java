import java.util.Scanner;

class StudentRecordTester 
{
	public static void main(String[] args)
	{
		//***DECLERATION***//
		
		Scanner input;
		int quizScore1,quizScore2,quizScore3, midtermScore, finalScore;
		
		StudentRecord myRecord;
		
		//***INITIALIZATION***//
		
		myRecord = new StudentRecord;
		input = new Scanner(System.in);
		
		quizScore1 = 0;
		quizScore2 = 0;
		quizScore3 = 0;
		
		midtermScore = 0;
		
		finalScore = 0;
		
		
		//***INPUT***//
		
		System.out.println("Welcome to the Student Record." + "/n" +
						   "[-----------------------------]);
		
		//QUIZ 1
	
		System.out.println("Please enter score of Quiz 1:");
		
		quizScore1 = Integer.parseInt(input.nextLine());
		
		if(!myRecord.quizIsValid(quizScore1))
		{
			quizScore1 = myRecord.QUIZ_DEFAULT;
		}	
		//QUIZ 2

		System.out.println("Please enter score of Quiz 2:");

		quizScore2 = Integer.parseInt(input.nextLine());

		if(!myRecord.quizIsValid(quizScore2))
		{
			quizScore2 = myRecord.QUIZ_DEFAULT;
		}
		
		//QUIZ 3
		
		System.out.println("Please enter score of Quiz 3:");
		
		quizScore3 = Integer.parseInt(input.nextLine());
		
		if(!myRecord.quizIsValid(quizScore3))
		{
			quizScore3 = myRecord.QUIZ_DEFAULT;
		}
		
		//MIDTERM
		
		System.out.println("Please enter score of Midterm:");
		
		midtermScore = Integer.parseInt(input.nextLine());
		
		if(!myRecord.examIsValid(midtermScore))
		{
			midtermScore = myRecord.EXAM_DEFAULT;
		}
	
		//FINAL
		
		System.out.println("Please enter score of Final:");
		
		finalScore = Integer.parseInt(input.nextLine());
		
		if(!myRecord.examIsValid(finalScore))
		{
			finalScore = myRecord.EXAM_DEFAULT;
		}
	
		
		myRecord(quizScore1,quizScore2,quizScore3,midtermScore,finalScore);
			
		
		
	}
	
}
