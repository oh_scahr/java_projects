import java.util.Scanner;

public class ExamPr2
{
	public static void main(String[] args)
	{
		String poke1, poke2, poke3;
		int choice;
		Scanner input;
	
		input = new Scanner(System.in);
		
		poke1 = "Charmander";
		poke2 = "Bulbasaur";
		poke3 = "Squirtle";
		
		do
		{
		
			System.out.println("            Pokemon Java Version");
			System.out.println("                 Main Menu");
			System.out.println("1: " + poke1 + "\n2: " + poke2 + "\n3: " + 
							 	poke3 + "\n\n" +
							 	
							   "Enter your choice for your starter Pokemon: ");
						   
			choice = input.nextInt();
			
			input.close(); 
		
			switch (choice)
			{
				case 1: 
						System.out.println("Fire type, you want a challenge!");
				break;
				
				case 2:
						System.out.println("Grass type, you must be "+
											"a first timer...");
				break;
				
				case 3: 
						System.out.println("Water type, you want to have some" +
						" fun.");
				break;
				
				default:
						System.out.println("Please enter a nubmer 1 through 3"+
						" please.");
			}
		
		}while(choice <0 || choice >3);
	}
}