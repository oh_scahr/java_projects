/********************************************
* AUTHOR: <your name>
* COURSE:  CS 111 Intro to CS I
* SECTION:  <class days + time>
* HOMEWORK #:  <homework #, problem #>
* LAST MODIFIED:  <date of last change>
********************************************/
/*****************************************************************************
* <TITLE OF PROGRAM>
*****************************************************************************
* PROGRAM DESCRIPTION:
* <1-2 sentences describing overall program>
*****************************************************************************
* ALGORITHM:
* <Pseudocode here>
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
* <ex: Scanner= used for console input>
*****************************************************************************/
import java.util.Scanner; // for input.
import java.lang.Math; //to generate random number for jokes.

public  class MethodsProject
{
	public  static  void main(String[] args)
		{
			/***** DECLARATION SECTION *****/
			
			String joke1, joke2,joke3, menuPrompt,intro,temp;
			int rndmNum;
			char menuChoice;
			Scanner input;
			
			/***** INITIALIZATION SECTION *****/
			
			joke1 = "Why didn't the mushroom get a second date? Turns out he "+
					"was not a real fun-gi!";
			joke2 = "How many java developers does it take to screw a " +
					 "lightbulb? One, but with so many methods.";
			joke3 = "Sigh, Java isnt funny.";
					
			menuPrompt = "Menu\n" + "A: Tell me a joke.\n" + "B: Tell me " +
						 "average of an int and a double.\n" + "X: Exit\n" +
						 "Enter Choice:\n";
			
			intro = "Welcome user!";
			
			input = new Scanner(System.in);
			
			/***** INPUT SECTION *****/
			
			
			System.out.println(intro);
			
			do
			{
				System.out.print(menuPrompt);
				
				temp = input.nextLine();
				
				menuChoice = temp.charAt(0);
				
				switch(menuChoice)
				{
					case 'A':
					
					case 'a':
								rndmNum = (int)(Math.random() * 4 + 1);
								makeJoke(rndmNum, joke1, joke2, joke3);
					break;
					
					case 'B':
					case 'b':
								makeAvg(input);
					break;
					
					case 'X':
					case 'x': 
								System.exit(0);
					break;
					
					default :
								System.out.println("Invalid Answer! Please choose a valid option. Enter 'x' or 'X' to exit");
				}
				
			}while(menuChoice != 'x' || menuChoice != 'X');
			
			/***** PROCESSING SECTION *****/
			
			/***** OUTPUT SECTION *****/
			
		}
		
		
		public static void makeJoke(int rndm, String jokeA, String jokeB, String jokeC)
		{
			switch(rndm)
			{
				case 1:
						System.out.println(jokeA);
				break;
				
				case 2:
						System.out.println(jokeB);
				break;
				
				case 3: 
						System.out.println(jokeC);
				break;
				
				default:
						System.out.println("ERROR!");
						
			}
		}
		
		public static void makeAvg(Scanner scan, int numA, double numB)
		{
			System.out.println("Alright. Enter a whole integer: ");
			
			numA = Integer.parseInt(scan.NextLine());
			
			System.out.print("Ok.Now a decimal value:");
			
			numB = Double.parseDouble(scan.NextLine());
			
			double average = (numA + numB) / 2;
			
			System.out.print("The average for " + num1 + " and " + num2 + 
							 " is " + average);
			
		}
	
		
	
		
}