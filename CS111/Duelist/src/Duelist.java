public class Duelist
{
	public static final String DEFAULT_NAME = "Katniss";
	public static final double DEFAULT_ACCURACY = .50;
	
	private String name;
	private double accuracy; 
	private static int living = 0;
	private boolean isAlive;
	private int wins = 0;
	
	//DEFAULT CONSTRUCTOR
	public Duelist()
	{
		this.setName(DEFAULT_NAME);
		this.setAccuracy(DEFAULT_ACCURACY);
		this.isAlive = true;
		living ++; 
	}
	
	//CONSTRUCTOR
	public Duelist(String name, double accuracy)
	{
		this.setName(name);
		this.setAccuracy(accuracy);
		this.isAlive = true;
		living ++;
	}

	
	 /*********************************************
	 *DESCRIPTION: Sets name of duelist.
	 **********************************************
	 *PRECONDITIONS: Variables initialized. Name of
	 *				 type String is given.
	 **********************************************
	 *POSTCONDITIONS: Sets string name as the name 
	 *				  of the duelist.
	 *********************************************/
	 
	private void setName(String name)
	{
		this.name = name;
	}
	
	 /*********************************************
	 *DESCRIPTION: Sets accuracy of duelist.
	 *********************************************
	 *PRECONDITIONS: Variables initialized. Valid 
	 *				 accuracy value is given.
	 **********************************************
	 *POSTCONDITIONS: Sets double accuracy given 
	 *				  as the accuracy of the duelist.
	 *********************************************/
	
	private void setAccuracy(double accuracy)
	{
		//valid check
		if(accuracy > 1 || accuracy < 0)
		{
			this.accuracy = DEFAULT_ACCURACY;
		}
		else
		{
			this.accuracy = accuracy;
		}
	}
	
	 /************************************************
	 *DESCRIPTION: Gets name of duelist.
	 *************************************************
	 *PRECONDITIONS: Name of type String is initialized.
	 **********************************************
	 *POSTCONDITIONS: Returns name.
	 ************************************************/
	
	public String getName()
	{
		return name;
	}
	
	 /*********************************************
	 *DESCRIPTION: Gets accuracy of duelist.
	 *********************************************
	 *PRECONDITIONS: Valid accuracy is initialized.
	 **********************************************
	 *POSTCONDITIONS: Return accuracy.
	 *********************************************/
	
	public double getAccuracy()
	{
		return accuracy;
	}
	
	 /*********************************************
	 *DESCRIPTION: Gets the number of duelists living.
	 *********************************************
	 *PRECONDITIONS: Variable living is initialized.
	 **********************************************
	 *POSTCONDITIONS: Returns living.
	 *********************************************/
	 
	 public static int getLiving()
	 {
	 	return living;
	 }
	 
	 /*********************************************
	 *DESCRIPTION: Gets number of wins.
	 *********************************************
	 *PRECONDITIONS: Variable wins is initialized.
	 **********************************************
	 *POSTCONDITIONS: Returns wins.
	 *********************************************/
	 
	 public int getWins()
	 {
	 	return wins;
	 }
	 
	 /*********************************************
	 *DESCRIPTION: Gets status if Duelist is alive.
	 *********************************************
	 *PRECONDITIONS: Variable isAlive is initialized.
	 **********************************************
	 *POSTCONDITIONS: Returns isAlive.
	 *********************************************/
	 public boolean isLiving()
	 {
	 	return isAlive;
	 }
	 
	 /*********************************************
	 *DESCRIPTION: Shoots at specified target.
	 *********************************************
	 *PRECONDITIONS: An object duelist must be initialized
	 *				 and given.
	 **********************************************
	 *POSTCONDITIONS: Sets target boolean isAlive either 
	 *				  true or false.
	 *********************************************/
	 
	 public void shoot(Duelist target)
	 {
	 	if(Math.random() < accuracy)
	 	{
	 		target.isAlive = false;
	 		living --;
	 	}
	 }
	 
	 /*********************************************
	 *DESCRIPTION: Increments number of wins.
	 *********************************************
	 *PRECONDITIONS: Varibale wins is initialized.
	 **********************************************
	 *POSTCONDITIONS: Adds 1 to int wins.
	 *********************************************/
	 
	 public void addWins()
	 {
	 	wins++;
	 }
	 
	 /*********************************************
	 *DESCRIPTION: Displays Duelist stats/attributes.
	 *********************************************
	 *PRECONDITIONS: Duelist object is given.
	 **********************************************
	 *POSTCONDITIONS: Returns strings of duelist
	 *				  attributes.
	 *********************************************/
	 
	 public static String toString(Duelist duelist)
	 {
	 	return "The winner is " + duelist.name +"\n";
	 }
	 
	 /*********************************************
	 *DESCRIPTION: Checks if two Duelists are the same.		
	 *********************************************
	 *PRECONDITIONS: Two Duelists must be initialized.
	 **********************************************
	 *POSTCONDITIONS: Returns true or false.
	 *********************************************/
	 
	 public boolean equalsTo(Duelist duelist1, Duelist duelist2)
	 {
	 	return duelist1.equals(duelist2);
	 }
	 
	 /*********************************************
	 *DESCRIPTION: "Respawns" duelist.
	 *********************************************
	 *PRECONDITIONS: Duelist object is initialized.
	 *				 Variable lives is initialized.
	 **********************************************
	 *POSTCONDITIONS: Duelist's isAlive is set 
	 *				  to true and int living is 
	 *			      incremented by one.
	 *********************************************/
	 
	 public void respawn()
	 {
	 	this.isAlive = true;
	 	living ++;
	 }
	 
}