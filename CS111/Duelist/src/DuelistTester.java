/********************************************
* AUTHOR: <your name>
* COURSE:  CS 111 Intro to CS I
* SECTION:  <class days + time>
* HOMEWORK #:  <homework #, problem #>
* LAST MODIFIED:  <date of last change>
********************************************/
/*****************************************************************************
* Duelist of Puzzlevania
*****************************************************************************
* PROGRAM DESCRIPTION:
* This program will test a group of duelists to determine who is the best 
* puzzeler based on total number of rounds won between a duel.
*****************************************************************************
* ALGORITHM:
* 
* DELCARE: Duelist Bob
* DECLARE: Duelist Aaron
* DECLARE: Duelist Celest
*
* INITIALIZE: Bob = new Duelist("Bob", 1.0/3)
* INITIALIZE: Aaron = new Duelist("Aaron",1.0/2)
* INITIALIZE: Celest = new Duelist("Celest", 1.0/1)
*
* PRINTLN: "Contendants are "
* PRINTLN: Bobs name
* PRINTLN: Aarons name
* PRINTLN: Celests name
*
* PRINTLN: "May the odds be in your favor!"
*
* FOR int rounds = 0, rounds < 10000, rounds++
*
*	  DO 
*		 IF Bob.isAlive
*		    IF Celest.isAlive
*				CALL Bob.shoot(Celest)
*			ELSE IF Aaron.isAlive
*			 	CALL Bob.shoot(Aaron)
*           END IF
*        END IF
*
*        IF Aaron.isAlive
*			IF Celest.isALive
*			    CALL Aaron.shoot(Celest)
*           ELSE IF Bob.isAlive
*				CALL Aaron.shoot(Bob)
*           END IF
*        END IF
*
*        IF Celest.isAlive
*			IF Aaron.isAlive
*				CALL Celest.shoot(Aaron)
*			ELSE IF Bob.isAlive
*				CALL Celest.shoot(Bob)
*           END IF
*        END IF
*     WHILE living != 1
* END FOR
*
* IF Bob.isLiving()
*	 CALL Bob.addWins()
* ELSE IF Aaron.isLiving()
*	 CALL Aaron.addWins()
* ELSE IF Celest.isLiving()
*	 CALL Celest.addWins()
* END IF
*
* CALL Bob.respawn()
* CALL Aaron.respawn()
* CALL Celest.respawn()
*
* CALL Cs111.printHeader(10,"Current Date")
*
* IF Bob.getWins() > Aaron.getWins() && Bob.getWins() > Celest.getWins()
* 	 PRINTLN Duelist.toString(Bob)
* END IF
*
* IF Aaron.getWins() > Celest.getWins && Aaron.getWins() > Bob.getWins()
* 	 PRINTLN Duelist.toString(Aaron)
* END IF
*
* IF Celest.getWins() > Aaron.getWins && Celest.getWins() > Bob.getWIns()
* 	 PRINTLN Duelist.toString(Celest)
* END IF
*
* PRINTF "Bob has won "+Bob.getWins()+" rounds or .2f %%", CALC Bob.getWins/ 
*		   10000
* PRINTF "Aaron has won " + Aaron.getWins() + " rounds or .2f %%", CALC 
*		   Aaron.getWins() / 10000 
* PRINTF "Celest has won " + Celest.getWins() + "rounds or .2f %%", CALC
*		   Celest.getWins()/10000
*		
*
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
* No extra packages are needed.
*****************************************************************************/

public class DuelistTester
{
	public static void main(String[] args)
	{
		//DECLERATION
		
		Duelist Aaron;
		Duelist Bob;
		Duelist Celest;
		
		//INITIALIZATION
		
		Aaron = new Duelist("Aaron", 1.0/4);
		Bob = new Duelist("Bob", 1.0/3);
		Celest = new Duelist("Celest",1.0/1);
		
		//PROCESSING
		
		for(int rounds = 0; rounds < 10000; rounds++)
		{	
			do
			{
				if(Bob.isLiving())
				{
					if(Celest.isLiving())
					{
						Bob.shoot(Celest);
					}
					else 
					{
						Bob.shoot(Aaron);
					}
				}
				
				if (Aaron.isLiving())
				{
					if(Celest.isLiving())
					{
						Aaron.shoot(Celest);
					}
					else
					{
						Aaron.shoot(Bob);
					}	
				}
				
				if (Celest.isLiving())
				{
					if(Aaron.isLiving())
					{
						Celest.shoot(Aaron);
					}
					else
					{
						Celest.shoot(Bob);
					}
				}
			}while(Duelist.getLiving() != 1);
			
			if(Bob.isLiving())
			{
				Bob.addWins();
			}
			else if(Aaron.isLiving())
			{
				Aaron.addWins();
			}
			else if(Celest.isLiving())
			{
				Celest.addWins();
			}
			
			Bob.respawn();
			Aaron.respawn();
			Celest.respawn();
		}

		//OUTPUT
		
		Cs111.printHeader(10, "April 13,2015");
		
		System.out.println("DUUUUUUUUEEEEEEEL! Contendants are: " +
						   "\n");
		System.out.println(Bob.getName());
		System.out.println(Aaron.getName());
		System.out.println(Celest.getName());
		
	    System.out.println( "\n" + "May the odds be in your favor!" + "\n");
		
		
		if(Bob.getWins() > Celest.getWins() &&
		   Bob.getWins() > Aaron.getWins())
		{
			System.out.println(Duelist.toString(Bob));
		}
		if(Aaron.getWins() > Celest.getWins() && 
		   Aaron.getWins() > Bob.getWins())
		{
			System.out.println(Duelist.toString(Aaron));
		}
		if(Celest.getWins() > Bob.getWins() &&
		   Celest.getWins() > Aaron.getWins())
		{
			System.out.println(Duelist.toString(Celest));
		}
		
		System.out.printf("Bob has won " + Bob.getWins() + 
						  "/10000 or %.2f"+"%% \n",(((double)Bob.getWins()/
						   10000)*100));
		System.out.printf("Aaron has won " + Aaron.getWins() + 
						 "/10000 or %.2f" + "%% \n",(((double)Aaron.getWins()/
						   10000)*100));
		System.out.printf("Celest has won "+Celest.getWins() + 
						 "/10000 or %.2f"+"%% \n",(((double)Celest.getWins()/
						  10000)*100));				   
	}
}