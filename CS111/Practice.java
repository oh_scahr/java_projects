import java.util.Scanner;

public class Practice{
    
    public static void main(String[] args) {
        
        Scanner keyboard;
           int intVal;
           double doubleVal;
           String stringVal, temp;
           
           keyboard = new Scanner(System.in);
           
           System.out.print("Please enter integer: ");
           temp = keyboard.nextLine();
           intVal = Integer.parseInt(temp);
    
        System.out.print("Please enter double: ");
        temp = keyboard.nextLine();
        doubleVal = Double.parseDouble(temp);
                
        System.out.print("Please enter string: ");
        stringVal = keyboard.nextLine();
                
        System.out.println("int = " + intVal 
                            + " double = " + doubleVal
                            + " string = " + stringVal);
    }
}