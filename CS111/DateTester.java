
public class DateTester {
    
    public static void main(String[] args) {
      
      Date today, bday, tomorrow;
      
      bday = new Date();
      today = new Date("March", 25, 2015);
      tomorrow = new Date("March");
      
      System.out.println(today);
      System.out.println(tomorrow);
      System.out.println(bday);
    }
}
