/********************************************
* AUTHOR: 						  Oscar Lopez
* COURSE:                CS 111 Intro to CS I
* SECTION:  	  		   M/W 11:05AM-1:05PM
* HOMEWORK #:  			Homework 3, Project 3
********************************************/
/*****************************************************************************
* Nickel-Budget Vending Machine
*****************************************************************************
* PROGRAM DESCRIPTION:
* 
* This program will act as a vending machine calculating change by user amount
* and the price of the item he/she desires.
*
*****************************************************************************
* ALGORITHM:
*
* 1)User enters dollar amount (divisible by 5).
* 2)User enters price of item he/she desires.(No greater than 1.00 and also 
*	divisible by 5).
* 3)Program then calcualtes change by subtracting amount entered by the price
*	of the item. 
* 4)Change will be calculated by multiplying each coin amount (quarter,dime,
*	nickel) by the amount given and after each coin the amount of those coins
*	in change will be taken off the total change.
* 5)Change will be displayed.
*
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
* java.util.Scanner - For user input of price and amount tendered.
* java.text.DecimalFormat - formats coins to whole numbers.
*****************************************************************************/
// <IMPORTS GO HERE>
import java.util.Scanner;
import java.text.DecimalFormat;

public  class VendingMachine

{	
	public static final double QUARTERS = 0.25;
	public static final double DIMES = 0.10;
	public static final double NICKELS = .05;
	
	

	public  static  void main(String[] args)
	{
		/***** DECLARATION SECTION *****/
		String price, moneyGiven;
		Double change, quarters, dimes, nickels;
		DecimalFormat changeFormat = new DecimalFormat("#0.#");
		
		/***** INITIALIZATION/INPUT SECTION *****/
		System.out.println("Welcome to Nickel-Budget Snack Bar! Enter the price of the item you desire:");
		
		Scanner input = new Scanner(System.in);
		price = input.nextLine();
		
		System.out.println("Cool! Please enter the amount you will place in the machine:");
		
		Scanner input2 = new Scanner(System.in);
		moneyGiven = input2.nextLine();
		
		/***** PROCESSING SECTION *****/
		change = Double.parseDouble(moneyGiven) - Double.parseDouble(price);
		
		quarters = change / QUARTERS;
		
		change = change - (quarters * QUARTERS);
		
		dimes = change / DIMES;
		
		change = change - (dimes * DIMES);
		
		nickels = change / NICKELS;
		
		change = change - (nickels * NICKELS);
		
		/***** OUTPUT SECTION *****/
		
		System.out.println("Thanks for your purchase! Your change is as follows:");
		System.out.println("");
		System.out.println("Quarters: " + changeFormat.format(quarters));
		System.out.println("Dimes: " + changeFormat.format(dimes));
		System.out.println("Nickels: " + changeFormat.format(nickels));
	}
}