/********************************************
* AUTHOR: Oscar Lopez				
* COURSE:  CS 111 Intro to CS I
* SECTION:  M/W 11:05pm-1:05pm
* HOMEWORK #:  Homework #3,Project 1
********************************************/
/*****************************************************************************
* Mortgage Payment Summary
*****************************************************************************
* PROGRAM DESCRIPTION:
* Gives a brief summary of a mortage loan after an initial monthly payment.
*****************************************************************************
* ALGORITHM:
* 	1.Have user enter name.
	2.Have user enter monthly payment.
	3.Have user enter outstanding balance.
	4.Calculate monthly interest payments

		-interestPayment = (Annual Interest / 12) * monthly payment.
		-formatted (%,.2f)
	
	5.Calculate monthly principal.

		-principal = monthly payment - interestPaid.
		-formatted(%,.2f)

	6.Calculate New Balance.
	
		-New Balance = Outstanding balance - principal.
		-formatted(%,.2f)

	7.Output interestPayment and principal.

	8.Output Outstanding balance.

	
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
* java.util.Scanner - Enables user to input monthly payment and balance.
*****************************************************************************/
// <IMPORTS GO HERE>
import java.util.Scanner;

public  class MortgageProject 
{
	public static final double ANNUAL_INTEREST_RATE = .0749;
	
	public static final String HEADER_SPEC_COLUMN1 = ("%-18s");
	public static final String HEADER_SPEC_COLUMN2 = ("%-18s");
	public static final String HEADER_SPEC_COLUMN3 = ("%-15s");
	public static final String HEADER_SPEC_COLUMN4 = ("%-22s");
	public static final String ROW1_SPEC_COLUMN1 = ("%-,17.2f");
	public static final String ROW1_SPEC_COLUMN2 = ("%-,17.2f");
	public static final String ROW1_SPEC_COLUMN3 = ("%-,14.2f");
	public static final String ROW1_SPEC_COLUMN4 = ("%,21.2f");
	
	public  static  void main(String[] args)
	{
		/***** DECLARATION SECTION *****/
		String monthlyPayment,outstandingBalance, monthlyPaymentHeader,
			   principalHeader,interestHeader,balanceHeader;
		
		double interestPayment,monthlyPrincipal,newBalance;
	
		
		/***** INITIALIZATION SECTION/INPUT *****/
		
		monthlyPaymentHeader = "Monthly Payment";
		principalHeader = "Principal";
		interestHeader = "Interest";
		balanceHeader = "Balance Due";
		
		System.out.println("What is your monthly payment?");
		Scanner input2 = new Scanner(System.in);
		monthlyPayment = input2.nextLine();
		
		System.out.println("What is your mortgage balance?");
		Scanner input3 = new Scanner(System.in);
		outstandingBalance = input3.nextLine();
		
		/***** PROCESSING SECTION *****/
		interestPayment = (ANNUAL_INTEREST_RATE / 12) * 
		                   Integer.parseInt(monthlyPayment);
		
		monthlyPrincipal = Integer.parseInt(monthlyPayment) - interestPayment;
		
		newBalance =  Integer.parseInt(outstandingBalance) - monthlyPrincipal;
		
		/***** OUTPUT SECTION *****/
		
		System.out.println("|------------------|------------------|---------------|----------------------|");
		
		System.out.printf("|" + HEADER_SPEC_COLUMN1 + "|" + HEADER_SPEC_COLUMN2 + "|" + HEADER_SPEC_COLUMN3 + "|" + HEADER_SPEC_COLUMN4 + "|\n",monthlyPaymentHeader,principalHeader,interestHeader,balanceHeader);
		
		System.out.println("|------------------|------------------|---------------|----------------------|");
						  
		System.out.printf("|$" + ROW1_SPEC_COLUMN1 + "|$" + ROW1_SPEC_COLUMN2 + "|$" + ROW1_SPEC_COLUMN3 + "|$" + ROW1_SPEC_COLUMN4 + "|\n", Double.parseDouble(monthlyPayment), monthlyPrincipal, interestPayment,newBalance);
		
		System.out.println("|------------------|------------------|---------------|----------------------|");
	}
}