
/********************************************
* AUTHOR:                         Oscar Lopez
* COURSE:                CS 111 Intro to CS I
* SECTION:               M/W 11:05am - 1:05pm
* HOMEWORK #:            Homework3, Project 2
********************************************/
/*****************************************************************************
* Sentence Rephraser
*****************************************************************************
* PROGRAM DESCRIPTION:
*
* This program will place the first word in the sentence the user types and 
* place it last.
*****************************************************************************
* ALGORITHM:
*
* 1)User enters a sentence without punctuation.
*
* 2)Program will read the first word of the sentence using substring and ' ' as
*   a delimiter.
*
* 3)The rest of the sentence will be a seperate substring.
*
* 4)After it has detected the delimiter, it will place the first substring and 
*   place it after the next substring in the sentence.
*
* 5)Program will then print the first substring and the next substring.
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
*  
* java.util.Scanner : Allows user to enter string in this program.
*****************************************************************************/
import java.util.Scanner;

public  class StringManipulation 
{
    public  static  void main(String[] args)
    {
        /***** DECLARATION SECTION *****/
        String firstWord, restOfSentence, sentence, newSentence;
        int x;
        
        /***** INITIALIZATION SECTION *****/
        System.out.println("Type any sentence without punctuation :");
        
        Scanner input = new Scanner(System.in);
        sentence = input.nextLine();
        
        x = sentence.indexOf(' ');
        
        firstWord = sentence.substring(0,x);
        
        restOfSentence = sentence.substring(x + 1);
        
        /***** PROCESSING SECTION *****/
        newSentence = restOfSentence.substring(0,1).toUpperCase() + restOfSentence.substring(1) 
        + " " + firstWord.substring(0,1).toLowerCase() + firstWord.substring(1);
        
        /***** OUTPUT SECTION *****/
        System.out.println( "I have rephrased that to say: " + newSentence);
        
    }
}