/*
        File Name:          CharacterCreator.java
        Programmer:         Oscar Lopez
        Date Last Modified: Dec 8 , 2015

        Problem Statement:
        This creates an extremely simple character creator with the options to create 9 additional
        characters.

        Overall Plan:
        1. Prompt intro screen.
        2. Prompt user for character name.
        3. Initialize name.
        4. Prompt user for character health.
        5. Initialize health.
        6. Prompt user for character base damage.
        7. Initialize damage.
        8. Prompt user for character Weapon type.
        9. Initialize weaponType.
        10.Prompt user for weapon damage.
        11.Initialize weaponDamage.
        12.Prompt user for character armor.
        13.Initialize armor.
        14.Prompt user for armor type.
        15.Initialize armor type.
        16.In an array type PlayerClass, set the following empty index to a new PlayerClass(name,health,damage,
           characterWeapon,characterArmor)
        17.Print character details.
        18.Prompt user if continue creating characters and amount of space left for characters to create.

        Classes needed and Purpose
        main class – CharacterCreator.java
        java.util.Scanner - For user input.
        Weapon.java - to create weapon.
        Armor.java - to create armor.
        Player.java - to create player name and id.
        PlayerClass.java - to create overall player info.
*/

import java.util.Scanner;

public class CharacterCreator
{
    public static void main(String[] args)
    {
        Scanner input;
        input = new Scanner(System.in);

        String name, armorType, weaponType;
        int damage,weaponDamage, health, armor, counter = 0;
        Weapon characterWeapon;
        Armor characterArmor;
        boolean done = false;
        PlayerClass[] players = new PlayerClass[10];

        System.out.println("_______________________________" +
                         "\n      CHARACTER CREATOR      " +
                         "\n-------------------------------");
        while(done == false)
        {
            System.out.println("Enter a name for the character:");
            name = input.nextLine();
            System.out.println("Enter some base health for the character:");
            health = input.nextInt();
            System.out.println("Enter amount of base damage this character will have:");
            damage = input.nextInt();
            String s = input.nextLine();
            System.out.println("What type of weapon will this character wield?");
            weaponType = input.nextLine();
            System.out.println("Enter amount of damage his/her weapon will deal");
            weaponDamage = input.nextInt();
            characterWeapon = new Weapon(weaponDamage, weaponType);
            System.out.println("Enter the amount of armor this character will have:");
            armor = input.nextInt();
            s = input.nextLine();
            if (armor <= 0) {
                characterArmor = new Armor();
            } else {
                System.out.println("Armor type: ");
                armorType = input.nextLine();
                if (armorType.isEmpty() || armorType.equals(null)) {
                    characterArmor = new Armor();
                } else {
                    characterArmor = new Armor(armor, armorType);
                }
            }
            System.out.println("Creating character");
            players[counter] = new PlayerClass(name, health, damage);
            players[counter].setPrimary(characterWeapon);
            players[counter].setArmor(characterArmor);
            System.out.println(players[counter].toString());
            counter++;

            if(counter == players.length)
            {
                done = true;
            }
            System.out.println("Create another character? There are " + (players.length - counter) + " slots left.");
            if (input.nextLine().equalsIgnoreCase("no")) {
                done = true;
            }
        }
    }
}