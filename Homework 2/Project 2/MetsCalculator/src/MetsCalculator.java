/*************************************************
 * AUTHOR:							   Oscar Lopez
 * COURSE:				        CS 111 M/W 11:05AM
 * HOMEWORK #:				 Homework 2, Project 2
 * LAST MODIFIED:					 July 26, 2015
 *************************************************/
 /**************************************************************************
 * METS Calculator
 ***************************************************************************
 * PROGRAM DESCRIPTION:
 *
 * This program will calculate the estimated number of calories a person will 
 * burn on three activities based on the weight the user enters.
 ***************************************************************************
 * ALGORITHM:
 *
 * DOUBLE DEFAULT_WEIGHT = 100.0
 *
 * DOUBLE runningMins = 30
 * DOUBLE basketballMins = 30
 * DOUBLE sleepingMins = 360
 *
 * INT RUNNING_METS = 10
 * INT BASKETBALL_METS = 8
 * INT SLEEPING_METS = 1
 *
 * DOUBLE weight
 * DOUBLE totalCaloriesBurned
 * DOUBLE caloriesBurnedRunning
 * DOUBLE caloriesBurnedBasketball
 * DOUBLE caloriesBurnedSleeping
 
 * 
 * PRINT "Please enter weight in kilograms: "
 * READ weight;
 * 
 * IF weight <= 0
 * DO weight = DEFAULT_WEIGHT
 * END IF
 *
 * CALC caloriesBurnedRunning = 0.0175 * RUNNING_METS * runningMins
 * CALC caloriesBurnedBasketball = 0.0175 * BASKETBALL_METS * basketballMins
 * CALC caloriesBurnedSleeping = 0.0175 * SLEEPING_METS * sleepingMins
 * CALC totalCaloriesBurned = caloriesBurnedRunning + caloriesBurnedBasketball
 *                            + caloriesBurnedSleeping
 *
 * PRINTLN "With the weight of " + weight + ", the person will have burned: "
 * PRINTLN "Running for " + runningMins + ": " + caloriesBurnedRunning 
 * PRINTLN "Playing Basketball for " + basketballMins + ": " +
 *          caloriesBurnedBasketball
 * PRINTLN "Sleeping for " + sleepingMins + ": " + caloriesBurnedSleeping
 *        
 ***************************************************************************
 * ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
 * java.util.Scanner : For user input and output.
 **************************************************************************/
 
 import java.util.Scanner;
 
 public class MetsCalculator
 {
 	public static void main(String[] args)
{
	double runningMins,basketballMins,sleepingMins,weight,totalCaloriesBurned,
		   caloriesBurnedRunning,caloriesBurnedBasketball,
		   caloriesBurnedSleeping;
		   
	Scanner input = new Scanner(System.in);
		   
	runningMins = 30;
	basketballMins = 30;
	sleepingMins = 360;
	
	System.out.println("Please enter a weight in kilograms: ");
	
	weight = Double.parseDouble(input.nextLine);
			
	
	
	
	
	
	
}	