/*************************************************
 * AUTHOR:
 * COURSE:
 * SECTION:
 * HOMEWORK #:
 * LAST MODIFIED:
 *************************************************/
 /**************************************************************************
 * Three Number Average
 ***************************************************************************
 * PROGRAM DESCRIPTION:
 *
 * This program will ask the user for three numbers and calculate the 
 * average of those given numbers.
 *
 ***************************************************************************
 * ALGORITHM:
 *
 * DECLARE: num1
 *			 num2
 *			 num3
 *			 average
 *			
 *			 scanner input
 *
 * PRINT: "Please enter three numbers (Hit enter after each number entered)"
 *
 * READ num1
 * READ num2
 * READ num3
 *
 * CALC: average = num1 + num2 + num3 / 3;
 *
 * PRINT: "Average of " num1 " and " num2 " and " num3 " is: " average
 ***************************************************************************
 * ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
 * java.util.Scanner : For user input and output.
 **************************************************************************/
 import java.util.Scanner;
 
 public class Hmwk2 
 {
    public static void main(String[] args) 
    {
    	int firstNumber, secondNumber, thirdNumber;
    	double average;
    	Scanner input = new Scanner(System.in);
    	
    	System.out.println("Please enter three numbers:");
    	
    	firstNumber = input.nextInt();
    	secondNumber = input.nextInt();
    	thirdNumber = input.nextInt();
    	
    	average = (firstNumber + secondNumber + thirdNumber)/ 3.0;
    	
    	System.out.println("The average of these numbers is : " + average);
    	
    }
}
