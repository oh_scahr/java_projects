public class HandGame 
{
    public static final int ROCK = 1;
    public static final int PAPER = 2;
    public static final int SCISSOR = 3;
    public static final int LIZARD = 4;
    public static final int SPOCK = 5;
    
    public static final String DEFAULT_NAME = "John Doe";
    
    private static int wins = 0;
    private static int losses = 0;
    private static int ties = 0;
    private static int rounds = 0;
    
    private static String name;
    
    //DEFAULT CONSTRUCTOR
    
    public void HandGamePlayer()
    {
    	setName(DEFAULT_NAME);
    }
    
    //CONSTRUCTOR
   
    public void HandGamePlayer(String name)
    {
    	setName(name);
    }
    
   
    /****************************************************
     *Description: Will print out sign based on hand sign
     ****************************************************
     *Preconditions: Sign must be given.
     ****************************************************
     *Postconditions: Prints out selection of hand.
     ***************************************************/
     
    public static void printHandSign(int sign)
    {
    	switch (sign)
    	{
    	
	    	case ROCK:
	    			 System.out.println("Rock");
	    	break;
	    	
	    	case PAPER: 
	    			System.out.println("Paper");
	    	break;
	    	
	    	case SCISSOR: 
	    			System.out.println("Scissor");
	    	break;
	    	
	    	case LIZARD:
	    			System.out.println("Lizard");
	    	break;
	    	
	    	case SPOCK:
	    			System.out.println("Spock");
	    	break;
	    	
	    	default: System.out.println("ERROR!");
    	}
    }
    //SETTERS
    
     private void setName(String name)
     {
     	if(name == "")
     	{
     		this.name = DEFAULT_NAME;
     	}
     	else
     	{
     		this.name = name;	
     	}
     	
     }
     
     /****************************************************
     *Description: Sets number of wins for the user.
     ****************************************************
     *Preconditions: Variables initialized. 
     ****************************************************
     *Postconditions: Accumulates number of wins by one.
     ***************************************************/
     
     public static void addWins()
     {
     	wins++;
     }
     
     /****************************************************
     *Description: Sets number of losses for the user.
     ****************************************************
     *Preconditions: Variables initialized. 
     ****************************************************
     *Postconditions: Accumulates number of losses by one.
     ***************************************************/
     
     public static void addLosses()
     {
     	losses++;
     }
     
     /****************************************************
     *Description: Sets number of ties for the user.
     ****************************************************
     *Preconditions: Variables initialized. 
     ****************************************************
     *Postconditions: Accumulates number of ties by one.
     ***************************************************/
     public static void addTies()
     {
     	ties++;
     }
     
     /****************************************************
     *Description: Sets number of rounds played.
     ****************************************************
     *Preconditions: Variables initialized. 
     ****************************************************
     *Postconditions: Accumulates rounds by one.
     ***************************************************/
     
     public static void addRounds()
     {
     	rounds++;
     }
     
     //GETTERS
     
     /****************************************************
     *Description: Makes a radomn hand selection.
     ****************************************************
     *Preconditions: Variables must be initialized.
     ****************************************************
     *Postconditions: Returns randomnly selected hand.
     ***************************************************/
    
    public static int getHandSign()
    {
    	int sign;
    	sign = (int)(Math.random() * 5) + 1;
    	return sign;
    }
    
     /****************************************************
     *Description: Collects stats of user.
     ****************************************************
     *Preconditions: Variables initialized. 
     ****************************************************
     *Postconditions: Returns win/losses ratio.
     ***************************************************/
     
     public static String getStats()
     {
     	return "Player Stats: " + wins + " wins / " + losses + " losses"
     			+ "\n" + "Ties: " + ties +"\n";
     }
     
     /****************************************************
     *Description: Gets name of curent player.
     ****************************************************
     *Preconditions: Variables initialized.
     ****************************************************
     *Postconditions: Returns name.
     ***************************************************/
     
     public static String getName()
     {
     	return name;
     }
     
     /****************************************************
     *Description: Gets number of wins.
     ****************************************************
     *Preconditions: Variables initialized.
     ****************************************************
     *Postconditions: Returns wins.
     ***************************************************/
     
     public static int getWins()
     {
     	return wins;
     }
     
     /****************************************************
     *Description: Gets number of losses.
     ****************************************************
     *Preconditions: Variables initialized.
     ****************************************************
     *Postconditions: Returns losses.
     ***************************************************/
     
     public static int getLosses()
     {
     	return losses;
     }
     
     public static int getTies()
     {
     	return ties;
     }
     
   
     /***************************************************
     *Description: Gets number of rounds.
     ****************************************************
     *Preconditions: Variables initialized. 
     ****************************************************
     *Postconditions: Returns rounds.
     ***************************************************/
     
     public static int getRounds()
     {
     	return rounds;
     }
     
     /***************************************************
     *Description: Checks equality of player names.
     ****************************************************
     *Preconditions: Variables initialized. 
     ****************************************************
     *Postconditions: Returns equal or notEqual.
     ***************************************************/
     
     public static boolean equals(HandGame player1,HandGame player2)
     {
     	return player1.equals(player2);
     }
     
     
     /***************************************************
     *Description: Declares Winner
     ****************************************************
     *Preconditions: Variables initialized. 
     *				 Wins and losses are set.
     ****************************************************
     *Postconditions: Returns result.
     ***************************************************/
     
     public static String declareWinner()
     {
     	String result;
     	
     	if(wins < losses)
     	{
     		result = "Skynet prevails! CPU wins!";
     	}
     	
     	else if(wins > losses)
     	{
     		result = "You beat the cpu! You win this game!";
     	}
     	else if (wins == losses)
     	{
     		result = "Tied Round!"; 
     	}
     	else
     	{
     		result = "ERROR!";
     		System.exit(0);
     	}
     	
     	return result;
     }
}