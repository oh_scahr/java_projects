import java.util.Scanner;

public class Cs111
{
	
	public static final String NAME = "Oscar Lopez";
	public static final String COURSE = "CS111 Intro to CS1";
	public static final String SECTION = "M/W 11:10AM - 1:05PM";
	
	/*************************************************************************
	 * DESCRIPTION:		Print author information for start of program
	 *************************************************************************
	 * PRE-CONDITIONS:	All parameters are given a value
	 *************************************************************************
	 * POST-CONDITIONS:	Outputs author info to console
	 ************************************************************************/
	 
	public static void printHeader(int homework, String lastModified) 
	{	
		System.out.println("/********************************************");
		System.out.println("* AUTHOR:        " + NAME);
		System.out.println("* COURSE:        " + COURSE);
		System.out.println("* SECTION:       " + SECTION);
		System.out.println("* HOMEWORK #:    " + homework);
		System.out.println("* LAST MODIFIED: " + lastModified);
		System.out.println("********************************************/");
	}
	
	
	/*****************************************************************************************
	* PSUEDOCODE:
	*
	* BEGIN int METHOD readInt PARAMETERS prompt, key, lower, upper
	* 	DO
	*		PROMPT prompt
	*		READ temp from key
	*		CALC result = parse integer from temp
	*		CALC isNotValid = result < lower OR result > upper
	*		IF isNotValid THEN
	*			OUTPUT "error message"
	*		END IF
	*	WHILE isNotValid
	*	RETURN result
	* END readInt
	*
	******************************************************************************************
	* DESCRIPTION:    Reads input from user, returns integer value (error-checked using bounds)
	* PRECONDITIONS:  key is instantiated, prompt is in form "Enter...: ", lower < upper
	* POSTCONDITIONS: Returns integer value between lower and upper (inclusive)
	******************************************************************************************/
	public static int readInt(String prompt, Scanner key, int lower, int upper)
	{
		String temp;
		int result;
		boolean isNotValid;
		
		do
		{
			System.out.print(prompt);
			temp = key.nextLine();
			result = Integer.parseInt(temp);
			isNotValid = (result < lower) || (result > upper);
			if(isNotValid)
			{
				System.out.println("ERROR: please enter value between " + lower + " - " + upper);
			}
		} while(isNotValid);
		
		return result;
	}
	
	/******************************************************************************************
	* PSEUDOCODE:
	* BEGIN double METHOD readDouble PARAMETERS prompt, key, lower, upper
	*	DO
	*		PROMPT prompt
	*		READ temp from key
	*		CALC result = parse double from temp
	*		CALC isNotValid = result < lower OR result > upper
	*		IF isNotValid THEN
	*			OUTPUT "error message"
	*		END IF
	*	WHILE isNotValid
	*	RETURN result
	*END readDouble
	*	
	******************************************************************************************
	* DESCRIPTION:    Reads input from user, returns double value (error-checked using bounds)
	* PRECONDITIONS:  key is instantiated, prompt is in form "Enter...: ", lower < upper
	* POSTCONDITIONS: Returns double value between lower and upper (inclusive)
	******************************************************************************************/
	public static double readDouble(String prompt, Scanner key, double lower, double upper)
	{
		String temp;
		double result;
		boolean isNotValid;
		
		do
		{
			System.out.print(prompt);
			temp = key.nextLine();
			result = Double.parseDouble(temp);
			isNotValid = (result < lower) || (result > upper);
			if(isNotValid)
			{
				System.out.println("ERROR: please enter value between " + lower + " - " + upper);
			}
		} while(isNotValid);
		
		return result;
		
	}
	
	
	
	/******************************************************************************************
	* PSEUDOCODE:
	* BEGIN char METHOD readChar PARAMETERS prompt, key, validChars
	*	DO
	*		PROMPT prompt
	*		READ temp from key
	*		CALC result = temp.charAt(0)
	*		CALC isNotValid = (validChars.indexOf(result) == -1)
	*		IF isNotValid THEN
	*			OUTPUT "error message"
	*		END IF
	*	WHILE isNotValid
	*	RETURN result
	*	
	******************************************************************************************
	* DESCRIPTION:    Reads input from user, returns char value (error-checked using validChars)
	* PRECONDITIONS:  key is instantiated, prompt is is form "Enter...: ", validChars
	* POSTCONDITIONS: Returns char value from validChars
	******************************************************************************************/
    public static char readChar(String prompt, Scanner key, String validChars)
    {
    	String temp;
    	char result;
    	boolean isNotValid;
    	
    	do 
    	{
    		System.out.print(prompt);
    		temp = key.nextLine();
    		result = temp.charAt(0);
    		isNotValid = (validChars.indexOf(result) == -1);
    		if (isNotValid)
    		{
    			System.out.println("ERROR: please enter a character between " + validChars);
    		}
    	} while (isNotValid);
    	 
        return result;
        
    }
    
    //ADDED
    
    public static String promptName(Scanner input)
    {
    	String name;
    	
    	input = new Scanner(System.in);
    	System.out.println("Enter your name: ");
    	name = input.nextLine();
    	System.out.println("\n");
    	return name;
    }
		
}