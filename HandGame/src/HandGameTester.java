
import java.util.Scanner;

class HandGameTester 
{
	public static void main(String[] args)
	{
		//DECLERATION
		Scanner keyboard;
		
		int userSign;
		int compSign;
		HandGame player;
		String userName;
		
		//INIIALIZE
		keyboard = new Scanner(System.in);
		player = new HandGame();
		
		//PRINTING MAIN HEADER
		Cs111.printHeader(8, "April 5, 2015"); 
		
		//INPUT
		
		player.HandGamePlayer(Cs111.promptName(keyboard));
		
		
		System.out.println("Welcome " + HandGame.getName() + "\n");
		
		do
		{
			userSign = Cs111.readInt("Enter hand sign"
								  + " (1 for Rock, 2 for Paper, 3 for Scissors" 
								  + ", 4 for Lizard, 5 for Spock, " 
								  + "6 to Exit Game:",keyboard,1,6);
				  
			if(userSign == 6)
			{
				System.out.println(HandGame.getStats());
				System.out.println("Rounds Played: " + HandGame.getRounds());
				System.out.println("\n" + HandGame.declareWinner());
				System.exit(0);
			}
			
			compSign = HandGame.getHandSign();
			
			//PROCESSING
			System.out.println("Your hand was ");
			HandGame.printHandSign(userSign);
			System.out.println("Computer's hand was "); 
			HandGame.printHandSign(compSign);
			
			//OUTPUT
			if(userSign == compSign)
			{
				System.out.println("Tie");
				HandGame.addTies();
			}
			
			else if (userSign == HandGame.ROCK && compSign == HandGame.PAPER ||
					 compSign == HandGame.SPOCK)
			{
				System.out.println("You Lose :(");
				HandGame.addLosses();
			}
			
			else if(userSign == HandGame.ROCK && compSign == HandGame.SCISSOR ||
					compSign == HandGame.LIZARD)
			{
				System.out.println("You win! :D");
				HandGame.addWins();
			}
			
			else if(userSign == HandGame.PAPER && compSign == HandGame.ROCK ||
					compSign == HandGame.SPOCK)
			{
				System.out.println("You win! :D");
				HandGame.addWins();
			}
			
			else if(userSign == HandGame.PAPER && compSign == HandGame.SCISSOR ||
					compSign == HandGame.LIZARD)
			{
				System.out.println("You Lose :(");
				HandGame.addLosses();
			}
			
			else if(userSign == HandGame.SCISSOR && compSign == HandGame.ROCK || 
					compSign == HandGame.SPOCK)
			{
				System.out.println("You Lose :(");
				HandGame.addLosses();
			}
			
			else if (userSign == HandGame.SCISSOR && compSign == HandGame.PAPER ||
					 compSign == HandGame.LIZARD)
			{
				System.out.println("You win! :D");
				HandGame.addWins();
			}
			
			else if(userSign == HandGame.LIZARD && compSign == HandGame.SCISSOR ||
					compSign == HandGame.ROCK)
			{
				System.out.println("You Lose :(");
				HandGame.addLosses();
			}
					
			else if(userSign == HandGame.LIZARD && compSign == HandGame.PAPER ||
					compSign == HandGame.SPOCK)
			{
				System.out.println("You Win! :D");
				HandGame.addWins();
			}
			
			else if(userSign == HandGame.SPOCK && compSign == HandGame.LIZARD || 
					compSign == HandGame.PAPER)
			{
				System.out.println("You lose :(");	
				HandGame.addLosses();	
			}
			
			System.out.print("\n");
			HandGame.addRounds();	
			System.out.println("Rounds so far: " + HandGame.getRounds());
			
		}while(userSign != 6);
	}
}
