/*
        File Name:          Weapon.java
        Programmer:         Oscar Lopez
        Date Last Modified: Dec 8 , 2015

        Problem Statement:
        This class will create any basic player class for a typical rpg style game.

        Overall Plan:
        1. Create a default constructor Weapon()
        2. Create a constructor with the arguments String name, int health, and int damage.
        3. Create copy constructor.
        4. Create method setHealth(int health)
        5. Create method setBaseDamage(int damage)
        6. Create method setName(String name)
        7. Create method setPrimary(Weapon weapon);
        8. Create method setSecondary(Weapon weapon);
        9. Create method setArmor(Armor armor);
        9. Create method getBaseHealth()
        10.Create method getBaseDamage()
        11.Create method getPrimary()
        12.Create method getSecondary()
        13.Create method getArmor()
        14.Create toString() method that will return int health, int baseDamage,primary weapon toString()
           and armor toString()
        15.Create equals(Object other) method.

        Classes needed and Purpose
        main class – CharacterCreator.java
        Player.java - base class.
        Weapon.java - needed info.(Part of PlayerClass Definition)
        Armor.java - needed info.(Part of PlayerClass Definition)
*/
public class PlayerClass extends Player
{
    public static final int DEFAULT_BASE_HEALTH = 100;
    public static final int DEFAULT_BASE_DAMAGE = 10;

    private int health;
    private int baseDamage;
    private Weapon primary;
    private Weapon secondary;
    private Armor armor;

    public PlayerClass()
    {
        super();
        setHealth(DEFAULT_BASE_HEALTH);
        setBaseDamage(DEFAULT_BASE_DAMAGE);
    }

    public PlayerClass(String name)
    {
        super(name);
        setHealth(DEFAULT_BASE_HEALTH);
        setBaseDamage(DEFAULT_BASE_DAMAGE);
    }

    public PlayerClass(int health)
    {
        super();
        setHealth(health);
        setBaseDamage(DEFAULT_BASE_DAMAGE);
    }

    public PlayerClass(int health, int damage)
    {
        super();
        setHealth(health);
        setBaseDamage(damage);
    }

    public PlayerClass(String name, int health)
    {
        super(name);
        setHealth(health);
    }

    public PlayerClass(String name, int health, int damage)
    {
        super(name);
        setHealth(health);
        setBaseDamage(damage);
    }

    private void setHealth(int health)
    {
        if (health <= DEFAULT_BASE_HEALTH && health > 0)
        {
            this.health = health;
        }
        else
        {
            System.out.println("\nInvalid Health value. Setting to default health.");
            this.health = DEFAULT_BASE_HEALTH;
        }
    }

    private void setBaseDamage(int damage)
    {
        if(damage >= 0)
        {
            baseDamage = damage;
        }
        else
        {
            System.out.println("\nInvalid damage value. Setting to default damage.");
            baseDamage = DEFAULT_BASE_DAMAGE;
        }
    }

    public void setPrimary(Weapon primary)
    {
        this.primary = primary;
    }

    public void setSecondary(Weapon secondary)
    {
        this.secondary = secondary;
    }

    public void setArmor(Armor armor)
    {
        this.armor = armor;
    }

    public Weapon getPrimary()
    {
        Weapon x = new Weapon(primary);
        return x;
    }

    public Weapon getSecondary()
    {
        Weapon y = new Weapon(secondary);
        return y;
    }

    public Armor getArmor()
    {
        Armor z = new Armor(armor);
        return z;
    }

    public int getBaseHealth()
    {
        return health;
    }

    public int getBaseDamage()
    {
        return baseDamage;
    }

    public String toString()
    {
        return("Player name: " + getName() + "\nPlayer ID: " + getPlayerId() +
               "\nBase Health: " + health + "\nBase Damage: " + getBaseDamage() +
               "\nPrimary Weapon: " + "\n     Damage: " + primary.getDamage() +
               "\n      Type: " + primary.getType()) + "\nArmor: " + getArmor();
    }

    public boolean equals(Object otherObject)
    {
        if(otherObject.getClass() != this.getClass())
        {
            return false;
        }

        PlayerClass otherPlayer = (PlayerClass)otherObject;

        return (getName().equals(otherPlayer.getName()) && getBaseHealth() == otherPlayer.getBaseHealth() &&
                getBaseDamage() == otherPlayer.getBaseDamage() && getPrimary() == otherPlayer.getPrimary() &&
                getSecondary() == otherPlayer.getSecondary());
    }
}