/********************************************
* AUTHOR: Oscar Lopez
* COURSE:  CS 111 Intro to CS I
* SECTION:  M/W 11:10AM - 1:05PM
* HOMEWORK #:  Homework7 Project1

********************************************/
/*****************************************************************************
* Odometer
*****************************************************************************
* PROGRAM DESCRIPTION:
* <1-2 sentences describing overall program>
*****************************************************************************
* ALGORITHM:
* 
* ASSIGN new odometer object "trip1"

  PROMPT for mpg

  INPUT mpg

  PROMPT for miles driven
  INPUT miles driven
  
  CALC addedMiles
  CALC fuelEfficiency.
  CALC fuelUsed.

  PRINTLN "For this trip you driven " + milesDriven + " at a fuel efficiency
  		   of " + mpg + 
*****************************************************************************
* ALL IMPORTED PACKAGES NEEDED AND PURPOSE:
* java.util.Scanner - for user input and output.
*****************************************************************************/
import java.util.Scanner;

public class OdometerTester
{
	
	public static void main(String[] args)
	{
		//***DECLERATON***//
		String mpg, milesDriven;
		Scanner input;
	
		trip1 = new Odometer();
		
		System.out.println("Please enter number of miles driven for this trip: ");
		input = new Scanner(System.in);
		
		milesDriven = String.parseInt(input.nextLine());
		
		System.out.println("Please enter your vehicles mpg:");
		
		mpg = String.parseInt(input.nextLine());
		
		int miles1 = trip1.addMiles(milesDriven);
		int mpg1 = trip1.setFuelEfficiency(mpg);
		
		double fuelUsed1 = trip1.getFuelUsed();
		
		System.out.println("For this trip you have driven: ");
		System.out.println( miles1 + " at a fuel efficiency of " + mpg 
							+ "mpg.");
		System.out.println("You have used " + fuelUsed1 + " gallons of fuel");
	}
}