public class Odometer
{
	/***CONSTANTS***/
	private static final int DEFAULT_MPG = 1; //Prevents a negative mpg value
											  //and division of zero.
	private static final int DEFAULT_MILES = 0;
	
	/***INSTANCE VARIABLES***/
	private int miles, fuelEfficiency;
	
	//DESCRIPTION: Returns integer argument as a string.
	//PRECONDITIONS: Given argument (miles1) must be an integer value.
	//POSTCONDITION: Will return given integer argument as a string.
	
	public String toString(int miles1)
	{
		
		return Integer.toString(miles1) ;
	}

	public void reset()
	{
		miles = 0;
	}
	
	//DESCRIPTION: Sets given mpg as fuelEfficiency.
	//PRECONDITION: mpg must be an integer value.
	//POSTCONDITION: Sets mpg as fuelEfficiency
	
	public void setFuelEfficiency(int mpg)
	{
		if(mpg > 0)
		{
			fuelEfficiency = mpg;
		}
		else
		{
			fuelEfficiency = DEFAULT_MPG;
		}
	}
	
	//DESCRIPTION: Adds given value(milesDriven) to Odometer. Also ensures 
	//			   value given is a valid value.
	//PRECONDITION: Given value must be an integer and greater than 
	//			    or equal to zero.
	//POSTCONDITION: Adds milesDriven to miles.
	
	public void addMiles(int milesDriven)
	{	
		if(milesDriven >= 0) //Prevents negative miles from being calculated.
		{
			miles = miles + milesDriven;
		}
		
		else
		{
			miles = DEFAULT_MILES;
		}
	}
	
	//DESCRIPTION: Calculates amount of fuel used and returns it.
	//PRECONDITION: FuelEfficiency must be greater than 0.
	//POSTCONDITION: Returns fuelUsed.
	
	public double getFuelUsed()
	{
		double fuelUsed;
		
		fuelUsed = miles / fuelEfficiency;
		return fuelUsed;
	}
	
	public boolean equals(String miles1, String miles2)
	{
		return miles1.equals(miles2);
	}

}