public class LetterCounter
{
	public static final char[] ALPHABET_ARRAY = {'A','B','C','D','E','F','G',
												 'H','I','J','K','L','M','N',
												 'O','P','Q','R','S','T','U',
												 'V','W','X','Y','Z'};	
										 
	//Assures counter array will always be equal to ALPHABET_ARRAY length.
	
	public static int[] counter = new int[ALPHABET_ARRAY.length]; 
	
	/*****************************************************
	 DESCRIPTION: Counts the number of characters used in
	 			  a sentence.
	 *****************************************************
	 PRE-CONDITIONS: String argument must be given and 
	 				 ALPHABET_ARRAY must be initialized.
	 				 String must end with any punctuation mark.
	 *****************************************************
	 POST-CONDITIONS: Sets counter array with int values
	 				  of each character used in the sentence.
	 ****************************************************/
	
	public static void countLetters(String input)
	{	
		int position;
		
		position = 0;
		
		while(position < input.length())
		{	
			//Converts current letter as a capitalized letter for 
			//comparison.
			
			char letter = Character.toUpperCase(input.charAt(position));
			
			//If common punctuation occurs, while loop SHOULD stop.
			
			if(letter != '.' || letter != '!' || letter !='?')
			{
					for(int i = 0; i < ALPHABET_ARRAY.length; i++)
					{
						if((int)letter - (int)ALPHABET_ARRAY[i] == 0)
						{
							counter[i]++;
						}
					}
					
				position++;
			}
			else
			{
				position = input.length();
			}
	    }
	}
	
	/*****************************************************
	 DESCRIPTION: Prints the counter array as a String.
	 *****************************************************
	 PRE-CONDITIONS: counter array and ALPHABET_ARRAY
	 				 must be initialized.
	 *****************************************************
	 POST-CONDITIONS: Prints out the ALPHABET_ARRAY and its
	 				  respective value from the counter array
	 				  formatted as several strings.
	 ****************************************************/
	
	public static void printCounter()
	{
		for(int i = 0; i < ALPHABET_ARRAY.length; i++)
		{
			if(counter[i] != 0)
			{
				System.out.println("Number of " + ALPHABET_ARRAY[i] + "'s : "
								   + counter[i]);
			}
		}
	}
	
	/*****************************************************
	 DESCRIPTION: Resets the counter Array.
	 *****************************************************
	 PRE-CONDITIONS: counter array must be initialized.
	 *****************************************************
	 POST-CONDITIONS: Sets all values of the counter Array 
	 				  to default zeros.
	 ****************************************************/
	 
	 public static void resetCounter()
	 {
	 	for(int i = 0; i < counter.length; i++)
	 	{
	 		counter[i] = 0;
	 	}
	 }
}




