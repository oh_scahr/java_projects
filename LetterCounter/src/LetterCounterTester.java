import java.util.Scanner;

public class LetterCounterTester
{
	public static void main(String[] args)
	{
		Scanner input;
		boolean continuing;
		
		input = new Scanner(System.in);
		continuing = true;
		
		while(continuing)
		{
			System.out.println("\nEnter a sentence with any end punctuation");
			
			LetterCounter.countLetters(input.nextLine());
			LetterCounter.printCounter();
			 
			System.out.println("\nContinue? 'Y' for yes. 'N' for No.");
			
			if(input.nextLine().equalsIgnoreCase("Y"))
			{
				LetterCounter.resetCounter();
			}
			else
			{
				continuing = false;
			}
		}	
	}
}