/*
        File Name:          Armor.java
        Programmer:         Oscar Lopez
        Date Last Modified: Dec 8 , 2015

        Problem Statement:
        This class will create any basic armor for a typical rpg style game.

        Overall Plan:
        1. Create a default constructor Armor()
        2. Create a constructor with the arguments int armor and String type.
        3. Create copy constructor.
        4. Create method setArmor(int damage)
        5. Create method setType(String type)
        6. Create method getArmor()
        7. Create method getType()
        8. Override toString and equals methods.
        9. toString() will return int armor and String type.

        Classes needed and Purpose
        main class – CharacterCreator.java
*/
public class Armor
{
    private int armor;
    String type;

    public Armor()
    {
        setArmor(0);
    }

    public Armor(int armor, String type)
    {
        setArmor(armor);
        setType(type);
    }

    public Armor(Armor otherArmor)
    {
        this(otherArmor.getArmor(),otherArmor.getType());
    }

    private void setArmor(int armor)
    {
        this.armor = armor;
    }

    private void setType(String type)
    {
        this.type = type;
    }

    public int getArmor()
    {
        return armor;
    }

    public String getType()
    {
        return type;
    }

    public String toString()
    {
        return "Armor : " + armor + "\nType: " + type;
    }

    public boolean equals(Object otherObject)
    {
        if(otherObject.getClass() != this.getClass())
        {
            return false;
        }
        else
        {
            Armor a = (Armor) otherObject;

            return(this.getArmor() == a.getArmor());
        }
    }
}
