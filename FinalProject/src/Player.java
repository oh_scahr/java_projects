/*
        File Name:          Player.java
        Programmer:         Oscar Lopez
        Date Last Modified: Dec 8 , 2015

        Problem Statement:
        This class will create the bare information any player in a typical rpg style game will have.

        Overall Plan:
        1. Create default constructor Player()
        2. Create constructor with arguments String name.
        3. Create method addPlayerId()
        4. Create method setPlayerName()
        5. Create method getPlayerName()
        6. Create method getPlayerId()
        7. Create toString() method that returns name and id number of player.
        8. Create an equals(Object other) method that will check equality with another player's name.

        Classes needed and Purpose
        main class – CharacterCreator.java
        java.text.DecimalFormat - Used to format player id.
*/
import java.text.DecimalFormat;

public class Player
{
    public static final int MAX_NUMBER_PLAYERS = 999999;
    public static final DecimalFormat ID_FORMAT = new DecimalFormat("000000");

    private int playerId;
    private static int numberOfPlayers = 0;
    private String name;

    public Player()
    {
        addPlayerId();
        setPlayerName("Default Player");
    }

    public Player(String name)
    {
        addPlayerId();
        setPlayerName(name);
    }

    private void addPlayerId()
    {
        if(numberOfPlayers < MAX_NUMBER_PLAYERS)
        {
            numberOfPlayers ++;
            playerId = numberOfPlayers;
        }
        else
        {
            System.out.println("ID Not Assigned. Number of Players is at" +
                               " full capacity.");
        }
    }

    private void setPlayerName(String playerName)
    {
        if (playerName.equals(null) || playerName.isEmpty())
        {
            System.out.println("Error in setting up Player name!");
        }
        else
        {
            name = playerName;
        }
    }

    public String getName()
    {
        return name;
    }

    public String getPlayerId()
    {
        String formattedId = ID_FORMAT.format(playerId);
        return formattedId;
    }

    public String toString()
    {
        String details = "Player: " + name + "\nPlayer ID: " +
                         getPlayerId();
        return details;
    }

    public boolean equals(Object otherObject)
    {
        if(otherObject.getClass() == this.getClass())
        {
            return false;
        }

        Player p = (Player)otherObject;

        return(this.getName().equals(p.getName()));
    }
}
