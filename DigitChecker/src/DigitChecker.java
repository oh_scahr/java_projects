public class DigitChecker
{	
	private static int[] accountNumber;
	
	private static int[] step1Array;
	private static int[] step2Array;
	private static int[] step3Array;
	
	public static void setUpAccountNumber(String number)
	{
		accountNumber = new int[number.length()];
		
		for(int i = 0; i < number.length(); i++)
		{
			accountNumber[i] = Integer.parseInt(number.substring(i,i+1).trim());
		}

	}

	public static void doubleEveryOtherDigit()
	{
		int[] step1Array = accountNumber.clone();
		
		for(int i = 0; i < accountNumber.length; i++)
		{
			if(i%2 != 0)
			{
				step1Array[i] *= 2;		
			}
		}
	}
		
	public static void  sumDoubleDigits()
	{
		int[] step2Array = step1Array.clone();
		
		for(int i = 0; i < accountNumber.length; i++)
		{
			if(step1Array[i] > 9)
			{
				step2Array[i] = (step1Array[i] %10) + (step1Array[i] /10);
				System.out.println(step2Array[i]);
			}
		}
	}
}
