import java.util.Scanner;

public class DigitCheckerTester
{
	public static void main(String[] args)
	{
		Scanner input;
		input = new Scanner(System.in);
		
		System.out.println("Please enter your account number:");
		
		DigitChecker.setUpAccountNumber(input.nextLine().trim());
		DigitChecker.doubleEveryOtherDigit();
		DigitChecker.sumDoubleDigits();
	}
}