/*
        File Name:          Weapon.java
        Programmer:         Oscar Lopez
        Date Last Modified: Dec 8 , 2015

        Problem Statement:
        This class will create any basic weapon for a typical rpg style game.

        Overall Plan:
        1. Create a default constructor.
        2. Create a constructor with the arguments int damage and String type.
        3. Create copy constructor.
        4. Create method setDamage(int damage)
        5. Create method setType(String type)
        6. Create method getDamage()
        7. Create method getType()
        8. Override toString and equals methods.
        9. toString() will return int damage and String type.

        Classes needed and Purpose
        main class – CharacterCreator.java
*/
public class Weapon
{
    public static final int DEFAULT_DAMAGE = 10;
    private int damage;
    private String type;

    public Weapon()
    {
        setDamage(10);
        setType("UNSPECIFIED");
    }

    public Weapon(int damage)
    {
        setDamage(damage);
        setType("UNSPECIFIED");
    }

    public Weapon(int damage, String type)
    {
        setDamage(damage);
        setType(type);
    }

    public Weapon(Weapon otherWeapon)
    {
        this(otherWeapon.getDamage(),otherWeapon.getType());
    }

    private void setDamage(int damage)
    {
        if(damage <= 0)
        {
            this.damage = DEFAULT_DAMAGE;
        }
        else
        {
            this.damage = damage;
        }
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getDamage()
    {
        return damage;
    }

    public String getType()
    {
        return type;
    }

    public String toString()
    {
        return "Weapon Damage: " + damage + "\nWeapon Type: " + type;
    }

    public boolean equals(Object otherObject)
    {
        if(this.getClass() != otherObject.getClass())
        {
            return false;
        }

        Weapon w = (Weapon)otherObject;

        return(damage == w.getDamage() && type.equals(w.getType()));

    }
}
