
/**
 * This is a tester file for the class Vehicle and class Truck.
 * 
 * @author (Oscar Lopez) 
 */
public class VehicleTester
{
    public static void main(String[] args)
    {
        Vehicle defaultVehicle = new Vehicle();
        System.out.println(defaultVehicle);
        
        Vehicle myVehicle = new Vehicle("Jeep",6,"Oscar");
        System.out.println(myVehicle);
        
        Truck defaultTruck = new Truck();
        System.out.println(defaultTruck);
        
        Truck someonesTruck = new Truck("Dodge",8,"Somebody Else's",6,2);
        System.out.println(someonesTruck);

        System.out.println(someonesTruck.equals(myVehicle));
    }
}
