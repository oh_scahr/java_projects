
/**
 * This class defines a truck in terms of load capacity and tow capacity.
 * 
 * @author (Oscar) 
 */
public class Truck extends Vehicle
{
    private int loadCap;
    private int towCap;

    public Truck()
    {
        super();
        setLoadCap(0);
        setTowCap(0);
    }

    public Truck(String manufacturer, int cylinders, String owner)
    {
        super(manufacturer,cylinders,owner);
        setLoadCap(0);
        setTowCap(0);
    }

    public Truck(String manufacturer, int cylinders, String owner, int loadCap)
    {
        super(manufacturer,cylinders,owner);
        setLoadCap(loadCap);
        setTowCap(0);
    }

    public Truck(String manufacturer, int cylinders, String owner, int loadCap, int towCap)
    {
        super(manufacturer,cylinders,owner);
        setLoadCap(loadCap);
        setTowCap(towCap);
    }
    
    public Truck(Truck other)
    {
        super(other.getManufacturer(),other.getCylinders(),other.getOwner());
        setLoadCap(other.getLoadCap());
        setTowCap(other.getTowCap());
    }
    
    //Mutators
    private void setLoadCap(int loadCap)
    {
        if(loadCap >= 0)
        {
            this.loadCap = loadCap;
        }
        else
        {
            System.out.println("Error in setting up loadCap.");
        }
    }
    
    private void setTowCap(int towCap)
    {
        if(towCap >= 0)
        {
            this.towCap = towCap;
        }
        else
        {
            System.out.println("Error in setting up TowCap.");
        }   
    }
    
    //Accessors.
    public int getLoadCap()
    {
        return loadCap;
    }
    
    public int getTowCap()
    {
        return towCap;
    }
    
    public String toString()
    {
        String details = ("Manufacturer Name: " + getManufacturer() + "\nCylinders: " +
                          getCylinders() + "\nOwner: " + getOwner() + "\nLoad Capacity: " +
                          getLoadCap() + "\nTow Capacity: " + getTowCap());
        return details;
    }
    
    public boolean equals(Object other)
    {
        if(this.getClass() != other.getClass())
        {
            return false;
        }
        
        Truck t = (Truck)other;
        
        return(getManufacturer().equals(t.getManufacturer()) && 
               getCylinders() == t.getCylinders() && getOwner().equals(t.getOwner())&&
               getLoadCap() == t.getLoadCap() && getTowCap() == t.getTowCap());
    }
}
