
/**
 * This class defines any vehicle with  a manufacturer, cylinders of engine, and an owner.
 * 
 * @author (Oscar Lopez)
 */
public class Vehicle
{
  private String manufacturer;
  private int numberOfCylinders;
  private String owner;
  
  public Vehicle()
  {
      setManufacturer("Government Vehicle");
      setCylinders(6);
      setOwner("Government");
  }
  
  public Vehicle(String manufacturer, int cylinders, String owner)
  {
      setManufacturer(manufacturer);
      setCylinders(cylinders);
      setOwner(owner);
  }
  
  public Vehicle(Vehicle other)
  {
      setManufacturer(other.getManufacturer());
      setCylinders(other.getCylinders());
      setOwner(other.getOwner());
  }
  
  //Mutators
  private void setManufacturer(String manufacturer)
  {
     if(manufacturer.equals(null) || manufacturer.isEmpty())
     {
         System.out.println("Error in setting up manufacturer.");
     }
     else
     {
        this.manufacturer = manufacturer;
     }
  }
  
  private void setCylinders(int cylinders)
  {
      if(cylinders > 0)
      {
          numberOfCylinders = cylinders;
      }
      else
      {
          System.out.println("Error in initializing number of cylinders");
      }
  }
  
  private void setOwner(String owner)
  {
      if(owner.equals(null)|| owner.isEmpty())
      {
          System.out.println("Error in initializing owner name.");
      }
      else
      {
          this.owner = owner;
      }
  }
  
  //Accessors
  public String getManufacturer()
  {
      return manufacturer;
  }
  
  public int getCylinders()
  {
      return numberOfCylinders;
  }
  
  public String getOwner()
  {
      return owner;
  }
  
  public boolean equals(Object other)
  {
      if(this.getClass() != other.getClass())
      {
          return false;
      }
      
      Vehicle v  = (Vehicle)other;
      
      return(manufacturer.equals(v.getManufacturer()) && 
             numberOfCylinders == v.getCylinders() &&
             owner.equals(v.getOwner()));
  }
  
  public String toString()
  {
      String details = "Owner : " + getOwner() + 
                       "\nManufacturer: " + getManufacturer() +
                       "\nNumber of Cylinders: " + getCylinders();
                       
      return details;
  }
}
